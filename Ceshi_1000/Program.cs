﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ceshi_1000
{
    public enum ECountry
    {
        China,
        US
    }
    [MessagePackObject]
    public class MyClass
    {
        // Key is serialization index, it is important for versioning.
        [Key(0)]
        public int Age { get; set; }

        [Key(1)]
        public string FirstName { get; set; }

        [Key(2)]
        public string LastName { get; set; }

        [Key(3)]
        public ECountry Contry { get; set; }

        // public members and does not serialize target, mark IgnoreMemberttribute
        [IgnoreMember]
        public string FullName { get { return FirstName + LastName; } }

        public static string Owner = "yhx";

        public MyClass() { }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var mc = new MyClass
            {
                Age = 99,
                FirstName = "hoge",
                LastName = "huga",
                Contry = ECountry.China
            };

            // call Serialize/Deserialize, that's all.
            var bytes = MessagePackSerializer.Serialize(mc);
            var mc2 = MessagePackSerializer.Deserialize<MyClass>(bytes);

            // you can dump msgpack binary to human readable json.
            // In default, MeesagePack for C# reduce property name information.
            // [99,"hoge","huga"]
            var json = MessagePackSerializer.ToJson(bytes);
            Console.WriteLine(json);

            Console.WriteLine($"Enum={ECountry.China}");
        }
    }
}
