﻿using MessagePack;
using System.Collections.Generic;
using UHan.fchinesechess;
using UHan.flobby;
using UHan.Network.enet;

public enum ERoomState
{
    Invalid = 0,
    Wait = 1,
    Ready = 2,
    Battle = 3,
    MAX = 4,
}

[MessagePackObject]
public class RoomVO
{
    [Key(0)]
    public uint Id { get; set; }

    [Key(1)]
    public ERoomState State { get; set; }

    [Key(2)]
    public List<PlayerVO> Players { get; set; }

    public static readonly int MAX_MEMBER_CNT = 2;

    public RoomVO()
    {
        Players = new List<PlayerVO>();
    }

    [IgnoreMember]
    public PlayerVO Master
    {
        get
        {
            if (Players.Count > 0)
            {
                return Players[0];
            }
            return null;
        }
    }

    [IgnoreMember]
    public PlayerVO Challenger
    {
        get
        {
            if (Players.Count > 1)
            {
                return Players[1];
            }
            return null;
        }
    }

    [IgnoreMember]
    public PlayerVO Applier;

    [IgnoreMember]
    public RoomMgr Mgr { get; set; }

    [IgnoreMember]
    public uint[] TokenArr
    {
        get
        {
            var arr = new uint[2];
            arr[0] = Players[0].token.Peer.ID;
            arr[1] = Players[1].token.Peer.ID;
            return arr;
        }
    }

    [IgnoreMember]
    public ChinessChessMatch match { get; set; }

    public void CreateBy(PlayerVO player)
    {
        State = ERoomState.Wait;
        player.RoomVO = this;
        Players.Clear();
        Players.Add(player);

        PlayerVO.OfflineEvent += OnOfflineHandler;
    }

    public void Join(PlayerVO player)
    {
        player.RoomVO = this;
        Players.Add(player);
        Applier = null;
    }

    private void OnOfflineHandler(PlayerVO player)
    {
        player.RoomVO?.Disband();
    }

    private void Disband()
    {
        PlayerVO.OfflineEvent -= OnOfflineHandler;
        foreach (var player in Players)
        {
            player.KickOut();
        }
        Mgr.Remove(Id);
    }
}
