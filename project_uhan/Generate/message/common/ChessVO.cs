﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[MessagePackObject]
public class ChessVO
{
    [Key(0)]
    public uint Id { get; set; }

    [Key(1)]
    public int X { get; set; }

    [Key(2)]
    public int Y { get; set; }

    public override string ToString()
    {
        return $"{{Id={Id},X={X},Y={Y}}}";
    }
}
