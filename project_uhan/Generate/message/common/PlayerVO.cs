﻿using ENet;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UHan;
using UHan.Network.enet;
using UKon;
using UKon.AI.ChineseChess;

[MessagePackObject]
public class PlayerVO: IAbstractChessPlayer
{
    [Key(0)]
    public uint Id { get; set; }
    [Key(1)]
    public string Name { get; set; }

    [IgnoreMember]
    public RoomVO RoomVO { get; set; }

    [IgnoreMember]
    public GameClient token { get; set; }

    [IgnoreMember]
    public EChessPieceFaction Faction { get; set; }

    public static Action<PlayerVO> OfflineEvent;

    public void KickOut()
    {
        OfflineEvent?.Invoke(this);
        RoomVO = null;

        DebugMgr.LogConsole(string.Format("KickOut {0}", Id));
        NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_KICK_OUT, token.Peer.ID,
               new byte[0], PacketFlags.Reliable);
    }

    public void QuitRoom()
    {

    }

    public void OffTurn()
    {
        DebugMgr.LogConsole("OffTurn");

        TurnNtf ntf_pack = new TurnNtf
        {
            isOnTurn = false
        };
        NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_TURN, token.Peer.ID,
               MessagePackSerializer.Serialize(ntf_pack), PacketFlags.Reliable);
    }

    public void OnTurn()
    {
        DebugMgr.LogConsole("OnTurn");

        TurnNtf ntf_pack = new TurnNtf
        {
            isOnTurn = true
        };
        NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_TURN, token.Peer.ID,
               MessagePackSerializer.Serialize(ntf_pack), PacketFlags.Reliable);
    }
}
