﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[MessagePackObject]
public class SelectNtf
{
    [Key(0)]
    public uint Chessid { get; set; }
}
