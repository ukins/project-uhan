﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[MessagePackObject]
public class TurnNtf
{
    [Key(0)]
    public bool isOnTurn { get; set; }
}
