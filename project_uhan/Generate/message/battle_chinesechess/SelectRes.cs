﻿using MessagePack;

[MessagePackObject]
public class SelectRes
{
    [Key(0)]
    public int ErrCode { get; set; }

    [Key(1)]
    public uint Chessid { get; set; }
}
