﻿using MessagePack;

[MessagePackObject]
public class MoveRes
{
    [Key(0)]
    public int ErrCode { get; set; }

    [Key(1)]
    public ChessVO VO { get; set; }
}
