﻿using MessagePack;

[MessagePackObject]
public class KillReq
{
    [Key(0)]
    public uint Attackerid { get; set; }

    [Key(1)]
    public uint Defenderid { get; set; }

    public override string ToString()
    {
        return $"{{Attackerid={Attackerid},DefenderId={Defenderid}}}";
    }
}
