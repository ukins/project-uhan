﻿using MessagePack;

[MessagePackObject]
public class KillRes
{
    [Key(0)]
    public int ErrCode { get; set; }

    [Key(1)]
    public uint Attackerid { get; set; }

    [Key(2)]
    public uint Defenderid { get; set; }

    public override string ToString()
    {
        return $"{{ErrCode={ErrCode},Attackerid={Attackerid},DefenderId={Defenderid}}}";
    }
}
