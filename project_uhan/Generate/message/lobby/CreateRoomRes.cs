﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[MessagePackObject]
public class CreateRoomRes
{
    [Key(0)]
    public int Errcode { get; set; }
    [Key(1)]
    public RoomVO VO { get; set; }

}
