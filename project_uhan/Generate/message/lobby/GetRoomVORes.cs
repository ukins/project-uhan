﻿using MessagePack;

[MessagePackObject]
public class GetRoomVORes
{
    [Key(0)]
    public int Errcode { get; set; }

    [Key(1)]
    public RoomVO VO { get; set; }
}
