﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[MessagePackObject]
public class DealJoinNtf
{
    [Key(0)]
    public bool isaccept { get; set; }

    [Key(1)]
    public PlayerVO VO { get; set; }
}
