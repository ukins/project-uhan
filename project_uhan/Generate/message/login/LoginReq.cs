﻿using MessagePack;

[MessagePackObject]
public class LoginReq
{
    [Key(0)]
    public string Name { get; set; }
    [Key(1)]
    public string Pwd { get; set; }
}
