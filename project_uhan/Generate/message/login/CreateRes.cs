﻿using MessagePack;

[MessagePackObject]
public class CreateRes
{
    [Key(0)]
    public int ErrCode { get; set; }
    [Key(1)]
    public uint PlayerId { get; set; }
}
