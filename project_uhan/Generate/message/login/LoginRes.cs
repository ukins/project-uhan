﻿using MessagePack;

[MessagePackObject]
public class LoginRes
{
    [Key(0)]
    public int ErrCode { get; set; }
    [Key(1)]
    public uint PlayerId { get; set; }
    [Key(2)]
    public string Name { get; set; }
    [Key(3)]
    public string Pwd { get; set; }
}
