using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class ErrCodeTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint Id { get { return m_id; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class ErrCodeTable : SingleKeyTable<ErrCodeTableConf,uint>
	{
		public override string Name { get { return "ErrCodeTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/errcode",
			@"flogin/table/errcode",
			@"flobby/table/errcode",
			@"fbattle_chinesechess/table/errcode",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ErrCodeTable' >
						<field name='Id' type='uint' primarykey='true' desc='错误码' />
						<field name='Desc' type='string' desc='错误描述' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
