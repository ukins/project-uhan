using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class ProtocolTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint Id { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }

		private EActor m_from;
		public EActor From { get { return m_from; } }

		private EActor m_to;
		public EActor To { get { return m_to; } }

		private bool m_islua;
		public bool isLua { get { return m_islua; } }

		private ProtoParam[] m_parameter;
		public ProtoParam[] Parameter { get { return m_parameter; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_name = br.ReadString();

				m_desc = br.ReadString();

				m_from = (EActor)Enum.Parse(typeof(EActor), br.ReadString());

				m_to = (EActor)Enum.Parse(typeof(EActor), br.ReadString());

				m_islua = br.ReadBoolean();

				int m_parameter_cnt = br.ReadInt32();
				m_parameter = new ProtoParam[m_parameter_cnt];
				for (int i = 0; i < m_parameter_cnt; ++i)
				{
					m_parameter[i] = new ProtoParam();
					m_parameter[i].Deserialize(br);
				}
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class ProtocolTable : SingleKeyTable<ProtocolTableConf,uint>
	{
		public override string Name { get { return "ProtocolTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/protocol",
			@"flogin/table/protocol",
			@"flobby/table/protocol",
			@"fbattle_chinesechess/table/protocol",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ProtocolTable' >
						<field name='Id' type='uint' primarykey='true' desc='协议id' />
						<field name='Name' type='string' desc='协议名称' />
						<field name='Desc' type='string' desc='协议描述' />
						<field name='From' type='EActor' desc='协议来源' />
						<field name='To' type='EActor' desc='协议去向' />
						<field name='isLua' type='bool' desc='Lua协议' />
						<field name='Parameter' type='array(ProtoParam)' isvertical='false' >
							<field name='Name' type='string' desc='参数名称' />
							<field name='Type' type='string' desc='参数类型' />
							<field name='Desc' type='string' desc='参数描述' />
						</field>
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
