using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;
using UKon.AI.ChineseChess;

namespace UKon.Table
{
	public class ChessmanTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint Id { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private EChessPieceType m_type;
		public EChessPieceType Type { get { return m_type; } }

		private UVector2Int m_birthpoint;
		public UVector2Int BirthPoint { get { return m_birthpoint; } }

		private string m_respath;
		public string ResPath { get { return m_respath; } }

		private string[] m_preload;
		public string[] Preload { get { return m_preload; } }

		private UVector3 m_lookat;
		public UVector3 LookAt { get { return m_lookat; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_name = br.ReadString();

				m_type = (EChessPieceType)Enum.Parse(typeof(EChessPieceType), br.ReadString());

				m_birthpoint = UVector2Int.Parse(br.ReadString());

				m_respath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				m_preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					m_preload[i] = br.ReadString();
				}

				m_lookat = UVector3.Parse(br.ReadString());
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class ChessmanTable : SingleKeyTable<ChessmanTableConf,uint>
	{
		public override string Name { get { return "ChessmanTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"fbattle_chinesechess/table/chessman",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ChessmanTable' >
						<field name='Id' type='uint' primarykey='true' desc='棋子索引' />
						<field name='Name' type='string' desc='棋子名称' />
						<field name='Type' type='EChessPieceType' desc='棋子类型' />
						<field name='BirthPoint' type='vector2int' desc='棋子出生点位' />
						<field name='ResPath' type='string' desc='模型资源' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
						<field name='LookAt' type='vector3' desc='棋子初始朝向' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
