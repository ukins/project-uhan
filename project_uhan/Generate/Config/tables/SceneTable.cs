using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class SceneTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_respath;
		public string ResPath { get { return m_respath; } }

		private string[] m_preload;
		public string[] Preload { get { return m_preload; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_name = br.ReadString();

				m_respath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				m_preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					m_preload[i] = br.ReadString();
				}
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class SceneTable : SingleKeyTable<SceneTableConf,uint>
	{
		public override string Name { get { return "SceneTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/scene",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='SceneTable' >
						<field name='ID' type='uint' primarykey='true' desc='场景Id' />
						<field name='Name' type='string' desc='场景名称' />
						<field name='ResPath' type='string' desc='资源路径' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
