using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class WndTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_luafile;
		public string LuaFile { get { return m_luafile; } }

		private string m_respath;
		public string ResPath { get { return m_respath; } }

		private string[] m_preload;
		public string[] Preload { get { return m_preload; } }

		private ESortingLayer m_layer;
		public ESortingLayer Layer { get { return m_layer; } }

		private ECacheType m_cachetype;
		public ECacheType CacheType { get { return m_cachetype; } }

		private uint m_relywnd;
		public uint RelyWnd { get { return m_relywnd; } }

		private bool m_ispopup;
		public bool IsPopup { get { return m_ispopup; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_name = br.ReadString();

				m_luafile = br.ReadString();

				m_respath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				m_preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					m_preload[i] = br.ReadString();
				}

				m_layer = (ESortingLayer)Enum.Parse(typeof(ESortingLayer), br.ReadString());

				m_cachetype = (ECacheType)Enum.Parse(typeof(ECacheType), br.ReadString());

				m_relywnd = br.ReadUInt32();

				m_ispopup = br.ReadBoolean();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class WndTable : SingleKeyTable<WndTableConf,uint>
	{
		public override string Name { get { return "WndTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/wnd",
			@"flogin/table/wnd",
			@"flobby/table/wnd",
			@"fbattle_chinesechess/table/wnd",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='WndTable' >
						<field name='ID' type='uint' primarykey='true' desc='窗口Id' />
						<field name='Name' type='string' desc='窗口名称' />
						<field name='LuaFile' type='string' desc='lua文件名称' />
						<field name='ResPath' type='string' desc='资源路径' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
						<field name='Layer' type='ESortingLayer' desc='层级' />
						<field name='CacheType' type='ECacheType' desc='缓存类型' />
						<field name='RelyWnd' type='uint' desc='依赖窗口' />
						<field name='IsPopup' type='bool' desc='是否弹出窗口' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
