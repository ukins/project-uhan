using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class LocalizeTableConf : SingleKeyItem<string>
	{
		public override string PrimaryKey { get { return m_keycode; } }

		private string m_keycode;
		public string KeyCode { get { return m_keycode; } }

		private string m_translation;
		public string Translation { get { return m_translation; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_keycode = br.ReadString();

				m_translation = br.ReadString();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class LocalizeTable : SingleKeyTable<LocalizeTableConf,string>
	{
		public override string Name { get { return "LocalizeTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/localize",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='LocalizeTable' >
						<field name='KeyCode' type='string' primarykey='true' desc='索引' />
						<field name='Translation' type='string' desc='翻译内容' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				string primarykey = key;
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				string range1 = min;
				string range2 = max;
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
