using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class FootholdTableConf : SingleKeyItem<UVector2Int>
	{
		public override UVector2Int PrimaryKey { get { return m_pos; } }

		private UVector2Int m_pos;
		public UVector2Int Pos { get { return m_pos; } }

		private Coordinate m_axis = new Coordinate();
		public Coordinate Axis { get { return m_axis; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_pos = UVector2Int.Parse(br.ReadString());

				m_axis.Deserialize(br);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class FootholdTable : SingleKeyTable<FootholdTableConf,UVector2Int>
	{
		public override string Name { get { return "FootholdTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"fbattle_chinesechess/table/foothold",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='FootholdTable' >
						<field name='Pos' type='vector2int' primarykey='true' desc='坐标点位' />
						<field name='Axis' type='Coordinate' isvertical='false' >
							<field name='X' type='float' desc='世界坐标X轴' />
							<field name='Y' type='float' desc='世界坐标Y轴' />
						</field>
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				UVector2Int primarykey = UVector2Int.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				UVector2Int range1 = UVector2Int.Parse(min);
				UVector2Int range2 = UVector2Int.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
