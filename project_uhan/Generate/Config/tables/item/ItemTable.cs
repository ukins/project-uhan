using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class ItemTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey { get { return m_id; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_name = br.ReadString();

				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class ItemTable : SingleKeyTable<ItemTableConf,uint>
	{
		public override string Name { get { return "ItemTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/item",
			@"flogin/table/item",
			@"flobby/table/item",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ItemTable' outputpath='item' >
						<field name='ID' type='uint' primarykey='true' desc='道具id' />
						<field name='Name' type='string' desc='道具名称' />
						<field name='Desc' type='string' desc='道具描述' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
