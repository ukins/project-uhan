using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class SkillTableConf : UnionKeyItem<uint,uint>
	{
		public override uint UnionKey1 { get { return m_skillid; } }

		public override uint UnionKey2 { get { return m_skilllv; } }

		private uint m_skillid;
		public uint SkillId { get { return m_skillid; } }

		private uint m_skilllv;
		public uint SkillLv { get { return m_skilllv; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_skillid = br.ReadUInt32();

				m_skilllv = br.ReadUInt32();

				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

	}

	public class SkillTable : UnionKeyTable<SkillTableConf,uint,uint>
	{
		private readonly string m_name = "SkillTable";
		public override string Name { get { return m_name; } }
		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/skill",
			@"flobby/table/skill",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='SkillTable' outputpath='item' >
						<field name='SkillId' type='uint' unionkey='true' desc='技能id' />
						<field name='SkillLv' type='uint' unionkey='true' desc='技能等级' />
						<field name='Desc' type='string' desc='技能描述' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key1, string key2)
		{
			try
			{
				uint unionkey1 = key1 == string.Empty ? 0 : UInt32.Parse(key1);
				uint unionkey2 = key2 == string.Empty ? 0 : UInt32.Parse(key2);
				return GenericFindVO(unionkey1, unionkey2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return null;
			}
		}
	}
}
