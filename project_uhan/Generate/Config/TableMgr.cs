using UKon.Table;

namespace UHan
{
	public partial class TableMgr
	{
		private readonly ItemTable m_ItemTable = new ItemTable();
		public ItemTable ItemTableVO { get { return m_ItemTable; } }

		private readonly AwardTable m_AwardTable = new AwardTable();
		public AwardTable AwardTableVO { get { return m_AwardTable; } }

		private readonly SkillTable m_SkillTable = new SkillTable();
		public SkillTable SkillTableVO { get { return m_SkillTable; } }

		private readonly WndTable m_WndTable = new WndTable();
		public WndTable WndTableVO { get { return m_WndTable; } }

		private readonly SceneTable m_SceneTable = new SceneTable();
		public SceneTable SceneTableVO { get { return m_SceneTable; } }

		private readonly LocalizeTable m_LocalizeTable = new LocalizeTable();
		public LocalizeTable LocalizeTableVO { get { return m_LocalizeTable; } }

		private readonly ProtocolTable m_ProtocolTable = new ProtocolTable();
		public ProtocolTable ProtocolTableVO { get { return m_ProtocolTable; } }

		private readonly ErrCodeTable m_ErrCodeTable = new ErrCodeTable();
		public ErrCodeTable ErrCodeTableVO { get { return m_ErrCodeTable; } }

		private readonly ChessmanTable m_ChessmanTable = new ChessmanTable();
		public ChessmanTable ChessmanTableVO { get { return m_ChessmanTable; } }

		private readonly FootholdTable m_FootholdTable = new FootholdTable();
		public FootholdTable FootholdTableVO { get { return m_FootholdTable; } }


		private void InnerInit()
		{
			m_tabledict.Add(m_ItemTable.Name, m_ItemTable);
			m_tablelist.Add(m_ItemTable);

			m_tabledict.Add(m_AwardTable.Name, m_AwardTable);
			m_tablelist.Add(m_AwardTable);

			m_tabledict.Add(m_SkillTable.Name, m_SkillTable);
			m_tablelist.Add(m_SkillTable);

			m_tabledict.Add(m_WndTable.Name, m_WndTable);
			m_tablelist.Add(m_WndTable);

			m_tabledict.Add(m_SceneTable.Name, m_SceneTable);
			m_tablelist.Add(m_SceneTable);

			m_tabledict.Add(m_LocalizeTable.Name, m_LocalizeTable);
			m_tablelist.Add(m_LocalizeTable);

			m_tabledict.Add(m_ProtocolTable.Name, m_ProtocolTable);
			m_tablelist.Add(m_ProtocolTable);

			m_tabledict.Add(m_ErrCodeTable.Name, m_ErrCodeTable);
			m_tablelist.Add(m_ErrCodeTable);

			m_tabledict.Add(m_ChessmanTable.Name, m_ChessmanTable);
			m_tablelist.Add(m_ChessmanTable);

			m_tabledict.Add(m_FootholdTable.Name, m_FootholdTable);
			m_tablelist.Add(m_FootholdTable);

		}
	}
}
