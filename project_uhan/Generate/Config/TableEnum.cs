using System;

namespace UKon.Table
{
	public enum ESortingLayer
	{
		// 无效值
		Invalid = 0,
		// 战斗UI层
		Battle = 1,
		// 战斗UI提示层
		BattleTips = 2,
		// 普通界面层
		Normal = 3,
		// 对话框层
		Dialog = 4,
		// 普通提示层
		Tips = 5,
		// 新手引导层
		Guide = 6,
		// 加载层
		Loading = 7,
		// 上限值
		MAX = 8,
	}

	public enum EActor
	{
		// 无效值
		Invalid = 0,
		// 客户端
		Client = 1,
		// 服务器
		Server = 2,
		// 上限值
		MAX = 3,
	}

	public enum ECacheType
	{
		// 不缓存
		Never = 0,
		// 缓存，依赖窗口出于现实状态则缓存，否则不缓存
		Normal = 1,
		// 始终缓存
		Always = 2,
		// 上限值
		MAX = 3,
	}

}
