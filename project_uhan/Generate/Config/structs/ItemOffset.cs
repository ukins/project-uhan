using System;
using System.IO;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class ItemOffset : AbstractTableStruct
	{
		private uint m_itemid;
		public uint ItemID { get { return m_itemid; } }

		private ItemTableConf m_itemid_conf;
		public ItemTableConf ItemIDConf
		{
			get
			{
				if (m_itemid_conf == null)
				{
					m_itemid_conf = TableMgr.Instance.ItemTableVO.GenericFindVO(ItemID);
				}
				return m_itemid_conf;
			}
		}

		private uint m_itemcnt;
		public uint ItemCnt { get { return m_itemcnt; } }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				m_itemid = br.ReadUInt32();
				m_itemid_conf = null;

				m_itemcnt = br.ReadUInt32();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

		public static ItemOffset CreateFromBinary(BinaryReader br)
		{
			var obj = new ItemOffset();
			obj.Deserialize(br);
			return obj;
		}
	}
}
