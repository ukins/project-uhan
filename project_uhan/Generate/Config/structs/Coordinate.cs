using System;
using System.IO;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class Coordinate : AbstractTableStruct
	{
		private float m_x;
		public float X { get { return m_x; } }

		private float m_y;
		public float Y { get { return m_y; } }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				m_x = br.ReadSingle();
				m_y = br.ReadSingle();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

		public static Coordinate CreateFromBinary(BinaryReader br)
		{
			var obj = new Coordinate();
			obj.Deserialize(br);
			return obj;
		}
	}
}
