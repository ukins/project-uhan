using System;
using System.IO;
using UKon.Config;
using UKon.Log;
using UHan;

namespace UKon.Table
{
	public class ProtoParam : AbstractTableStruct
	{
		private string m_name;
		public string Name { get { return m_name; } }

		private string m_type;
		public string Type { get { return m_type; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				m_name = br.ReadString();
				m_type = br.ReadString();
				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				LogMgr.Instance.LogError(e.ToString());
				return false;
			}
			return true;
		}

		public static ProtoParam CreateFromBinary(BinaryReader br)
		{
			var obj = new ProtoParam();
			obj.Deserialize(br);
			return obj;
		}
	}
}
