using UKon.Network;
using System.Collections.Generic;

namespace UKon.Proto
{
	public partial class CCRoomVO : IAbstractProtoVO
	{
		/// <summary>
		/// 房间id
		/// <summary>
		public uint Roomid;

		/// <summary>
		/// 房间状态
		/// <summary>
		public ERoomState Roomstate;

		/// <summary>
		/// 房间状态
		/// <summary>
		public List<CCPlayerVO> Players;

		public void Encode(ByteStream stream)
		{
			stream.Write(Roomid);
			stream.Write((int)Roomstate);
			stream.Write(Players.Count);
			for (int i = 0; i < Players.Count; i++)
			{
				Players[i].Encode(stream);
			}
		}

		public void Decode(ByteStream stream)
		{
			Roomid = stream.ReadUInt32();
			Roomstate = (ERoomState)stream.ReadInt32();
			int len = stream.ReadInt32();
			Players = new List<CCPlayerVO>(len); 
			for (int i = 0; i < len; i++)
			{
				Players.Add(new CCPlayerVO());
				Players[i].Decode(stream);
			}
		}
	}
}
