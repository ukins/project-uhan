using UKon.Network;

namespace UKon.Proto
{
	public class ChessVO : IAbstractProtoVO
	{
		/// <summary>
		/// 棋子id
		/// <summary>
		public uint id;

		/// <summary>
		/// 棋子x坐标
		/// <summary>
		public int x;

		/// <summary>
		/// 棋子y坐标
		/// <summary>
		public int y;

		public void Encode(ByteStream stream)
		{
			stream.Write(id);
			stream.Write(x);
			stream.Write(y);
		}

		public void Decode(ByteStream stream)
		{
			id = stream.ReadUInt32();
			x = stream.ReadInt32();
			y = stream.ReadInt32();
		}
	}
}
