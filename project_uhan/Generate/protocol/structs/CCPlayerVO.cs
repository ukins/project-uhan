using UKon.Network;

namespace UKon.Proto
{
	public partial class CCPlayerVO : IAbstractProtoVO
	{
		/// <summary>
		/// 玩家ID
		/// <summary>
		public uint PlayerId;

		/// <summary>
		/// 玩家名字
		/// <summary>
		public string PlayerName;

		public void Encode(ByteStream stream)
		{
			stream.Write(PlayerId);
			stream.Write(PlayerName);
		}

		public void Decode(ByteStream stream)
		{
			PlayerId = stream.ReadUInt32();
			PlayerName = stream.ReadString();
		}
	}
}
