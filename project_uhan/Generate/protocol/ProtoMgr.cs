using UKon.Proto;

namespace UHan
{
	public partial class ProtoMgr
	{
		private void InnerInit()
		{
			m_dict.Add("CCPlayerVO", new CustomProtoVODecoder<CCPlayerVO>());
			m_dict.Add("CCRoomVO", new CustomProtoVODecoder<CCRoomVO>());
			m_dict.Add("ChessVO", new CustomProtoVODecoder<ChessVO>());
			m_dict.Add("ERoomState", new ProtoERoomStateDecoder(EProtoDecoderType.Simple));
			m_dict.Add("list(CCPlayerVO)", new CustomProtoVOListDecoder<CCPlayerVO>());
			m_dict.Add("list(CCRoomVO)", new CustomProtoVOListDecoder<CCRoomVO>());
		}
	}
}
