using UKon.Network;
using System;
using System.Collections.Generic;

namespace UKon.Proto
{
	public enum ERoomState
	{
		Invalid = 0,
		Wait = 1,
		Ready = 2,
		Battle = 3,
		MAX = 4,
	}

	public class ProtoERoomStateDecoder : AbstractProtoDecoder
	{
		private EProtoDecoderType m_type = EProtoDecoderType.Simple;
		public ProtoERoomStateDecoder(EProtoDecoderType type)
		{
			m_type = type;
		}

		public override object Decode(ByteStream stream)
		{
			switch (m_type)
			{
				case EProtoDecoderType.Simple:
					return stream.ReadInt32();
				case EProtoDecoderType.List:
					{
						int len = stream.ReadInt32();
						List<ERoomState> lst = new List<ERoomState>(len);
						for (int i = 0; i < len; i++)
						{
							lst.Add((ERoomState)stream.ReadInt32());
						}
						return lst;
					}
				case EProtoDecoderType.Array:
					{
						int len = stream.ReadInt32();
						ERoomState[] arr = new ERoomState[len];
						for (int i = 0; i < len; i++)
						{
							arr[i] = (ERoomState)stream.ReadInt32();
						}
						return arr;
					}
				default: break;
			}
			throw new Exception("ProtoERoomStateDecoder .Decode Error!");
		}
	}
}
