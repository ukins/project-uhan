namespace UKon
{
	/// <summary>
	/// generic协议枚举 Range[0,10000)
	/// <summary>
	public enum EGenericProto
	{
		NTF_NET_CONNECT = 1000,
		NTF_NET_DISCONNECT = 1001,
		NTF_NET_TIMEOUT = 1002,
    }

	/// <summary>
	/// login协议枚举 Range[10000,20000)
	/// <summary>
	public enum ELoginProto
	{
		REQ_LOGIN = 10000,
		RES_LOGIN = 10001,
		REQ_CREATE = 10002,
		RES_CREATE = 10003,
		REQ_SYNC = 10100,
		RES_SYNC = 10101,
	}

	/// <summary>
	/// lobby协议枚举 Range[20000,30000)
	/// <summary>
	public enum ELobbyProto
	{
		REQ_GET_ROOM_LIST = 20000,
		RES_GET_ROOM_LIST = 20001,
		REQ_GET_ROOM = 20010,
		RES_GET_ROOM = 20011,
		REQ_CREATE = 20020,
		RES_CREATE = 20021,
		REQ_QUIT_ROOM = 20025,
		RES_QUIT_ROOM = 20026,
		REQ_JOIN = 20030,
		RES_JOIN = 20031,
		NTF_JOIN = 20032,
		REQ_DEAL_JOIN = 20040,
		RES_DEAL_JOIN = 20041,
		NTF_DEAL_JOIN = 20042,
		NTF_START_MATCH = 20051,
	}

	/// <summary>
	/// chinesechess协议枚举 Range[30000,40000)
	/// <summary>
	public enum EChinesechessProto
	{
		REQ_SELECT = 30000,
		RES_SELECT = 30001,
		NTF_SELECT = 30002,
		REQ_MOVE = 30010,
		RES_MOVE = 30011,
		NTF_MOVE = 30012,
		REQ_KILL = 30020,
		RES_KILL = 30021,
		NTF_KILL = 30022,
		NTF_TURN = 30030,
		NTF_KICK_OUT = 30040,
		NTF_GAME_OVER = 30050,
	}

}
