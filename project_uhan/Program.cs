﻿using System;
using System.Diagnostics;
using System.Threading;
using UHan.fchinesechess;
using UHan.flobby;
using UHan.flogin;

namespace UHan
{
    class Program
    {
        static void Main(string[] args)
        {
            FeatureMgr featureMgr = FeatureMgr.Instance;

            featureMgr.Register(TimeMgr.Instance);
            featureMgr.Register(DebugMgr.Instance);
            featureMgr.Register(NetworkMgr.Instance);
            featureMgr.Register(DatabaseMgr.Instance);
            featureMgr.Register(TableMgr.Instance);

            featureMgr.Register(new LoginFeature());
            featureMgr.Register(new LobbyFeature());
            featureMgr.Register(new BattleFeature());

            //while (true)
            //{
            //    Console.WriteLine("please input order:");
            //    string command = Console.ReadLine();
            //    if (command == "q" || command == "quit")
            //    {
            //        featureMgr.UnregisterAll();
            //        break;
            //    }
            //    DebugMgr.Log(command);
            //}

            //Console.WriteLine("Server Quit Successfully! ByeBye...");

            int frameRate = 30;
            int frameMilliseconds = 1000 / frameRate;

            Stopwatch stopwatch = new Stopwatch();
            int overTime = 0;
            while (true)
            {
                stopwatch.Restart();

                //root.Step((frameMilliseconds + overTime) * 0.001f);
                TimeMgr.Instance.Update();

                stopwatch.Stop();

                int stepTime = (int)stopwatch.ElapsedMilliseconds;

                if (stepTime <= frameMilliseconds)
                {
                    Thread.Sleep(frameMilliseconds - stepTime);
                    overTime = 0;
                }
                else
                {
                    overTime = stepTime - frameMilliseconds;
                }
            }
        }
    }
}
