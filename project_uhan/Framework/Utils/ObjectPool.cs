﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ObjectPool
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 21:17:27
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Utils
{
    public interface IObjectPool
    {
        void Clear();
    }

    public abstract class ObjectPool<T> : IObjectPool
        where T : IObjectPool, new()
    {
        private static Stack<T> m_pool = new Stack<T>();
        public static T Pop()
        {
            if (m_pool.Count > 0)
            {
                return m_pool.Pop();
            }
            return new T();
        }
        public static void Push(T o)
        {
            if (m_pool.Contains(o) == false)
            {
                o.Clear();
                m_pool.Push(o);
            }
        }

        public static void Resize(uint cnt)
        {
            while (m_pool.Count > cnt)
            {
                m_pool.Pop();
            }
        }

        public abstract void Clear();
    }
}
