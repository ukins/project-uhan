﻿using System.Collections.Generic;
using System.Linq;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：DictContainer
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 22:49:32
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Common
{
    public class DictContainer<K, T> : IDictContainer<K, T>
         where T : IContainerElement<K>
    {
        protected Dictionary<K, T> m_dict = new Dictionary<K, T>();

        public T this[K key]
        {
            get
            {
                m_dict.TryGetValue(key, out T value);
                return value;
            }
        }

        public int Count => m_dict.Count;

        public List<T> FindElements()
        {
            return m_dict.Values.ToList();
        }

        public DictContainer()
        {
            m_dict.Clear();
        }

        public virtual void Add(T value)
        {
            if (m_dict.ContainsKey(value.Id) == true)
            {
                return;
            }
            //value.OnAdd();
            m_dict.Add(value.Id, value);
        }

        public virtual void Remove(K key)
        {
            if (m_dict.ContainsKey(key) == true)
            {
                //m_dict[key].OnRemove();
                m_dict.Remove(key);
            }
        }
    }
}
