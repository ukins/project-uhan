﻿namespace UHan.Common
{
    public interface IContainerElement<K>
    {
        K Id { get; }

    }
}
