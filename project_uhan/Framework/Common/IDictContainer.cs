﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UHan.Common
{
    interface IDictContainer<K, T>
        where T : IContainerElement<K>
    {
        T this[K key] { get; }

        int Count { get; }

        List<T> FindElements();

        void Add(T value);

        void Remove(K key);
    }
}
