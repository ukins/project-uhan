﻿using MySql.Data.MySqlClient;
using System;
using UKon;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：DatabaseMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 22:21:41
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public class DatabaseMgr : Singleton<DatabaseMgr>, IFeature
    {
        public string Name => "DatabaseMgr";

        private MySqlConnection m_conn;

        public void install()
        {
            DebugMgr.LogConsole("DatabaseMgr install succ");

            string connStr = "server = localhost; user = root; database = test; port = 3306; password = 123456";
            m_conn = new MySqlConnection(connStr);
            try
            {
                m_conn.Open();
            }
            catch (Exception ex)
            {
                DebugMgr.LogConsole(ex.ToString());
            }

            DebugMgr.LogConsole("数据库连接成功");
        }

        public void uninstall()
        {
            DebugMgr.LogConsole("DatabaseMgr uninstall succ");
        }

        public MySqlDataReader ExecuteReader(string sql)
        {
            MySqlCommand cmd = new MySqlCommand(sql, m_conn);
            return cmd.ExecuteReader();
        }

        public int ExecuteNonQuery(string sql)
        {
            MySqlCommand cmd = new MySqlCommand(sql, m_conn);
            return cmd.ExecuteNonQuery();
        }

        public object ExecuteScalar(string sql)
        {
            MySqlCommand cmd = new MySqlCommand(sql, m_conn);
            return cmd.ExecuteScalar();
        }
    }
}
