﻿using UKon;
using UKon.Cmd;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：CmdMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/10 16:21:44
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public class CmdMgr : Singleton<CmdMgr>, IFeature
    {

        public string Name => "CmdMgr";
        public void install()
        {
            DebugMgr.LogConsole("CmdMgr install succ");
        }

        public void uninstall()
        {
            DebugMgr.LogConsole("CmdMgr uninstall succ");
        }

        private OrderMgr m_orderMgr = new OrderMgr();

        public void Register(AbstractOrder order)
        {
            m_orderMgr.Register(order);
        }

        public void UnRegister(AbstractOrder order)
        {
            m_orderMgr.UnRegister(order);
        }

        public void Process(string cmd)
        {
            m_orderMgr.Process(cmd);
        }

    }
}
