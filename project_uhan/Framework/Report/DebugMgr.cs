﻿using System;
using System.IO;
using UKon;
using UKon.Timer;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：LogMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 22:17:53
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public class DebugMgr : Singleton<DebugMgr>, IFeature
    {
        public string Name => "LogMgr";

        private string m_datetime;
        private TimeTicker m_timer;
        private static StreamWriter m_streamwriter;

        public void install()
        {
            m_datetime = DateTime.Now.ToShortDateString();
            string filename = string.Format("DailyRecord/{0}.txt", m_datetime);
            FileInfo fi = new FileInfo(filename);
            if (fi.Directory.Exists == false)
            {
                fi.Directory.Create();
            }
            if (fi.Exists == false)
            {
                fi.Create();
            }
            //m_streamwriter = fi.CreateText();
            m_streamwriter = fi.AppendText();

            m_timer = new TimeTicker(OnTimeTickHandler, 1000 * 3);
            TimeMgr.Instance.AddTick(m_timer);
            m_timer.Start();
            LogConsole("LogMgr install succ");
        }


        public void uninstall()
        {
            LogConsole("LogMgr uninstall succ");
            m_timer.Stop();
            m_streamwriter.Close();
        }

        public static void LogConsole(string str)
        {
            Console.WriteLine(str);
        }

        public static void Log(string str)
        {
            m_streamwriter.WriteLine(string.Format("{0}\t{1}", DateTime.Now.TimeOfDay, str));
            m_streamwriter.Flush();
        }

        #region Event Handler

        private void OnTimeTickHandler(bool iscomplete)
        {
            //LogConsole($"{DateTime.Now.TimeOfDay}: OnTimeTickHandler");
        }
        #endregion
    }
}
