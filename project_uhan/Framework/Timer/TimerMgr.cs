﻿using System;
using UKon;
using UKon.Timer;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：TimeMgr
// 文件功能描述：实现帧循环功能
// 后续增加掩饰执行功能
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 21:56:59
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    /// <summary>
    /// 帧循环时间管理器
    /// </summary>
    public class TimeMgr : AbstractTimeMgr, IFeature
    {
        #region Singleton
        private static TimeMgr m_instance;
        private static readonly object syslock = new object();

        public static TimeMgr Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (syslock)
                    {
                        if (m_instance == null)
                        {
                            m_instance = new TimeMgr();
                        }
                    }
                }
                return m_instance;
            }
        }
        private TimeMgr() { }

        #endregion

        public string Name => "TimeMgr";

        //private DateTimeOffset m_base_time;

        public event Action OnUpdate;

        public void install()
        {
            AbstractTimer.SetTimeMgr(Instance);

            //m_base_time = new DateTimeOffset(new DateTime(1970, 1, 1));
            //long currtime = Convert.ToInt64((DateTimeOffset.Now - m_base_time).TotalMilliseconds);
            long currtime = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000;
            Start(currtime);


            //NetworkMgr.Instance.OnTickEvent += Tick;
            DebugMgr.LogConsole("TimerMgr install succ");
        }

        public void uninstall()
        {
            Stop();

            //NetworkMgr.Instance.OnTickEvent -= Tick;

            DebugMgr.LogConsole("TimerMgr uninstall succ");
        }

        //public override void Tick()
        //{
        //    long currtime = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000;
        //    m_time.Tick(currtime);
        //}

        public void Update()
        {
            long currtime = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000;
            base.Tick(currtime);
            OnUpdate?.Invoke();
        }
    }
}
