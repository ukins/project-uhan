﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Config;
using UKon.Data.Table;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：TextParser
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/28 20:56:46
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Table
{
    public class TextParser
    {
        private List<IAbstractTable> TableList;
        private IAbstractTable m_table;
        private int m_idx = 0;

        private void ParseTextContent(List<string> assets)
        {
            //DebugMgr.Log(m_table.Name);
            CSVDataTable dt = new CSVDataTable(m_table.TableVO);
            foreach (var value in assets)
            {
                var subdt = new CSVDataTable(m_table.TableVO);
                subdt.Parse(value);
                dt.DataTable.Union(subdt.DataTable);
            }

            m_table.CSVDT = dt;

            MemoryStream stream = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(stream);
            dt.DataTable.Build(bw);
            stream.Flush();
            byte[] buffer = stream.GetBuffer();
            stream.Close();
            stream.Close();

            MemoryStream stream2 = new MemoryStream(buffer);
            BinaryReader br = new BinaryReader(stream2);
            m_table.Deserialize(br);
        }


        public void Parse()
        {
            //m_isall = true;
            TableList = TableMgr.Instance.TableList;
            if (TableList == null)
            {
                return;
            }

            m_idx = 0;

            m_table = TableList[m_idx];
            OnLoadTableCompete(LoadSubTable(TableList[m_idx].Path));
        }

        private List<string> LoadSubTable(List<string> paths)
        {
            List<string> m_values = new List<string>();

            for (int i = 0; i < paths.Count; i++)
            {
                string tablepath = Path.Combine(TableMgr.TableDir, paths[i] + ".csv");
                FileStream fs = new FileStream(tablepath, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs, Encoding.UTF8);
                m_values.Add(sr.ReadToEnd());
            }

            return m_values;
        }

        private void OnLoadTableCompete(List<string> assets)
        {
            ParseTextContent(assets);

            m_idx++;
            if (m_idx >= TableList.Count)
            {
                //onParseComplete?.Invoke();
            }
            else
            {
                m_table = TableList[m_idx];
                OnLoadTableCompete(LoadSubTable(TableList[m_idx].Path));
            }
        }

        public void Parse(string name)
        {
            //m_isall = false;
            m_table = TableMgr.Find(name);
            OnLoadSingleTableComplete(LoadSubTable(m_table.Path));
        }

        private void OnLoadSingleTableComplete(List<string> assets)
        {
            ParseTextContent(assets);
            //onParseComplete?.Invoke();

        }
    }
}
