﻿using System.Collections.Generic;
using System.Configuration;
using UHan.Table;
using UKon;
using UKon.Config;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：TableMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/28 17:02:20
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public partial class TableMgr : Singleton<TableMgr>, IFeature
    {
        public string Name => "TableMgr";

        private readonly List<IAbstractTable> m_tablelist = new List<IAbstractTable>();
        public List<IAbstractTable> TableList => m_tablelist;

        private readonly Dictionary<string, IAbstractTable> m_tabledict = new Dictionary<string, IAbstractTable>();

        private TextParser m_parser = new TextParser();

        public static readonly string TableDir;

        static TableMgr()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            TableDir = config.AppSettings.Settings["TableDir"].Value;
        }

        public void install()
        {

            InnerInit();

            ParseOnInit();

            DebugMgr.LogConsole("TableMgr install succ");
        }

        private void ParseOnInit()
        {
            m_parser.Parse();
            //if (File.Exists("../../config.txt") == true)
            //{
            //    DebugMgr.Log("exist");
            //}
            //else
            //{
            //    DebugMgr.Log("not exist");
            //}

            //DebugMgr.Log(Environment.CurrentDirectory);
        }

        public void uninstall()
        {
        }

        public static IAbstractTable Find(string str)
        {
            return Instance.InnerFind(str);
        }

        private IAbstractTable InnerFind(string str)
        {
            if (m_tabledict.ContainsKey(str) == false)
            {
                return null;
            }
            return m_tabledict[str];
        }

    }
}
