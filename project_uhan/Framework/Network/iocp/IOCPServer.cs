﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UKon.Network;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IOCPServer
// 文件功能描述：IOCP服务器
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 22:34:07
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Network.iocp
{
    /// <summary>
    /// IOCP服务器
    /// 参考：https://blog.csdn.net/zhujunxxxxx/article/details/43573879
    /// </summary>
    public class IOCPServer : IOCPConnect, IDisposable
    {
        #region Properties

        private int m_maxClientCount;

        private Socket m_acceptSocket;

        private int m_currClientCount;

        private Semaphore m_maxAcceptedSemaphore;

        private BufferManager m_bufferMgr;

        private AsyncUserTokenPool m_asyncUserTokenPool;

        //private bool isdisposed = false;

        private bool isinitialize = false;

        public bool IsRunning { get; private set; }

        public IPAddress Address { get; private set; }

        public int Port { get; private set; }

        public Encoding Encoding { get; private set; }

        #endregion

        #region Ctors

        public IOCPServer(int listenPort, int maxClient, int buffersize)
            : this(IPAddress.Any, listenPort, maxClient, buffersize)
        {

        }

        public IOCPServer(IPEndPoint localEP, int maxClient, int buffersize)
            : this(localEP.Address, localEP.Port, maxClient, buffersize)
        {

        }

        public IOCPServer(IPAddress localIPAddress, int listenPort, int maxClient, int buffersize)
        {
            this.Address = localIPAddress;
            this.Port = listenPort;
            this.Encoding = Encoding.Default;

            m_bufferSize = buffersize;
            m_maxClientCount = maxClient;

            m_acceptSocket = new Socket(localIPAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            m_bufferMgr = new BufferManager(m_bufferSize * m_maxClientCount * opsToPreAlloc, m_bufferSize);

            m_asyncUserTokenPool = new AsyncUserTokenPool(m_maxClientCount);

            m_maxAcceptedSemaphore = new Semaphore(m_maxClientCount, m_maxClientCount);
        }
        #endregion


        public void Init()
        {
            m_bufferMgr.InitBuffer();

            AsyncUserToken userToken;
            for (int i = 0; i < m_maxClientCount; i++)
            {
                userToken = new AsyncUserToken();
                userToken.RecvEventArgs.Completed += IO_Completed;
                userToken.SendEventArgs.Completed += IO_Completed;

                m_bufferMgr.SetBuffer(userToken.RecvEventArgs);
                m_bufferMgr.SetBuffer(userToken.SendEventArgs);

                m_asyncUserTokenPool.Push(userToken);
            }

            isinitialize = true;
        }

        public void Start()
        {
            if (!IsRunning)
            {
                if (isinitialize == false)
                {
                    Init();
                }
                IsRunning = true;

                IPEndPoint localEndPoint = new IPEndPoint(Address, Port);

                m_acceptSocket = new Socket(localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                if (localEndPoint.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    m_acceptSocket.SetSocketOption(SocketOptionLevel.IPv6, (SocketOptionName)27, false);
                    m_acceptSocket.Bind(new IPEndPoint(IPAddress.IPv6Any, localEndPoint.Port));
                }
                else
                {
                    m_acceptSocket.Bind(localEndPoint);
                }

                m_acceptSocket.Listen(m_maxClientCount);

                DebugMgr.LogConsole("服务器启动完成");
                DebugMgr.LogConsole(string.Format("ip:{0}; port:{1}", localEndPoint.Address, localEndPoint.Port));
                StartAccept(null);
            }
        }

        public void Stop()
        {
            if (IsRunning)
            {
                IsRunning = false;
                m_acceptSocket.Close();
            }
        }

        public void StartAccept(SocketAsyncEventArgs args)
        {
            if (args == null)
            {
                args = new SocketAsyncEventArgs();
                args.Completed += Accept_Completed;
            }
            else
            {
                args.AcceptSocket = null;
            }
            m_maxAcceptedSemaphore.WaitOne();
            if (m_acceptSocket.AcceptAsync(args) == false)
            {
                ProcessAccept(args);
            }
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                Socket s = e.AcceptSocket;
                if (s.Connected)
                {
                    try
                    {
                        Interlocked.Increment(ref m_currClientCount);
                        AsyncUserToken userToken = m_asyncUserTokenPool.Pop();
                        userToken.Socket = s;
                        userToken.ConnectDateTime = DateTime.Now;

                        var ipendpoint = (IPEndPoint)s.RemoteEndPoint;
                        DebugMgr.LogConsole(string.Format("new connect; ip={0}", ipendpoint.Address));

                        userToken.iIO.length = NetworkDefine.PackHeadSize;
                        userToken.iIO.offset = 0;

                        AsyncRecvHead(userToken);
                    }
                    catch (SocketException)
                    {
                        //e.AcceptSocket.Close();
                    }
                }
            }
            else
            {
                //e.AcceptSocket.Close();
            }

            StartAccept(e);
        }

        #region Event Handler

        private void Accept_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }



        #endregion

        protected override void CloseClientSocket(AsyncUserToken token)
        {
            base.CloseClientSocket(token);

            Interlocked.Decrement(ref m_currClientCount);
            m_maxAcceptedSemaphore.Release();

            m_bufferMgr.FreeBuffer(token.RecvEventArgs);
            m_bufferMgr.FreeBuffer(token.SendEventArgs);
            m_asyncUserTokenPool.Push(token);
        }

        public void Dispose()
        {
            //isdisposed = true;
        }
    }
}
