﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UKon.Network;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IOCPClient
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 22:34:07
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Network.iocp
{
    public class IOCPClient : IOCPConnect, IDisposable
    {
        private Socket m_clientSock;

        private static Mutex mutex = new Mutex();

        //private bool m_connected = false;

        //private const int ReceiveO
        private AsyncUserToken m_token;

        private IPEndPoint m_remoteEndPoint;

        public IOCPClient(IPEndPoint local, IPEndPoint remote)
        {
            m_clientSock = new Socket(local.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            m_remoteEndPoint = remote;

            var m_bufferMgr = new BufferManager(m_bufferSize * 1 * opsToPreAlloc, m_bufferSize);
            m_bufferMgr.InitBuffer();

            m_token = new AsyncUserToken();
            m_token.RecvEventArgs.Completed += IO_Completed;
            m_token.SendEventArgs.Completed += IO_Completed;

            m_bufferMgr.SetBuffer(m_token.RecvEventArgs);
            m_bufferMgr.SetBuffer(m_token.SendEventArgs);

            m_token.Socket = m_clientSock;
        }

        public void Connect()
        {
            SocketAsyncEventArgs connectArgs = new SocketAsyncEventArgs();

            connectArgs.UserToken = m_token;
            connectArgs.RemoteEndPoint = m_remoteEndPoint;
            connectArgs.Completed += ConnectArgs_Completed;

            if (m_clientSock.ConnectAsync(connectArgs) == false)
            {
                ProcessConnected(connectArgs);
            }
        }

        private void ConnectArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessConnected(e);
        }

        private void ProcessConnected(SocketAsyncEventArgs args)
        {
            mutex.ReleaseMutex();

            if (args.SocketError == SocketError.Success)
            {
                if (m_clientSock.Connected == true)
                {
                    //m_connected = true;

                    m_token.ConnectDateTime = DateTime.Now;

                    m_token.iIO.length = NetworkDefine.PackHeadSize;
                    m_token.iIO.offset = 0;
                    AsyncRecvHead(m_token);
                }
            }

        }

        public void Dispose()
        {
            mutex.Close();
        }
    }
}
