﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UHan.Network.iocp;
using UKon;
using UKon.Network;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：NetworkMgr
// 文件功能描述：网络管理器，处理数据包的接受与发送
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/23 21:54:17
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

    /// <summary>
    /// iocp备份
    /// </summary>
namespace UHan.Network.iocp
{
    public class NetworkMgr : Singleton<NetworkMgr>, IFeature
    {
        #region IFeature

        public string Name => "NetworkMgr";


        private IOCPServer m_server;

        public void install()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (int.TryParse(config.AppSettings.Settings["Port"].Value, out int port) == false)
            {
                port = 9999;
            }
            if (int.TryParse(config.AppSettings.Settings["MaxConnectionNum"].Value, out int maxConnectionNum) == false)
            {
                maxConnectionNum = 8000;
            }
            if (int.TryParse(config.AppSettings.Settings["BufferSize"].Value, out int bufferSize) == false)
            {
                bufferSize = 1024;
            }
            if (int.TryParse(config.AppSettings.Settings["SocketTimeOutMS"].Value, out int socketTimeOutMS) == false)
            {
                socketTimeOutMS = 5 * 60 * 1000;
            }

            m_server = new IOCPServer(port, maxConnectionNum, bufferSize);
            m_server.Start();

            DebugMgr.LogConsole("NetworkMgr install succ");
        }

        public void uninstall()
        {
            m_server.Stop();

            DebugMgr.LogConsole("NetworkMgr uninstall succ");
        }
        #endregion

        private Dictionary<UInt32, ProtoCallBack> m_protodict = new Dictionary<uint, ProtoCallBack>();

        public Action OnTickEvent;

        public void Register(UInt32 protoid, ProtoCallBack func)
        {
            if (protoid == 0 || func == null)
            {
                return;
            }
            if (m_protodict.ContainsKey(protoid) == false)
            {
                m_protodict.Add(protoid, func);
            }
            else
            {
                m_protodict[protoid] += func;
            }
        }

        public void Unregister(UInt32 protoid, ProtoCallBack func)
        {
            if (protoid == 0 || func == null)
            {
                return;
            }
            if (m_protodict.ContainsKey(protoid) == true)
            {
                m_protodict[protoid] -= func;
            }
        }

        public void Dispatch(UInt32 protoid, ByteStream byteArray, AsyncUserToken token)
        {
            if (m_protodict.ContainsKey(protoid) == true)
            {
                m_protodict[protoid]?.Invoke(byteArray, token);
            }
            OnTickEvent?.Invoke();
        }

        public bool Contains(UInt32 protoid)
        {
            return m_protodict.ContainsKey(protoid);
        }

        //public void SendMessage(UInt32 protoid, ByteStream byteArray, AsyncUserToken token)
        //{
        //    m_server.SendMessage(protoid, byteArray, token);
        //}

        //public void SendMessage(UInt32 protoid, ByteStream byteArray, AsyncUserToken[] tokens)
        //{
        //    for (int i = 0; i < tokens.Length; i++)
        //    {
        //        //lock (tokens[i])
        //        {
        //            byteArray.AddReferenceCount();
        //            m_server.SendMessage(protoid, byteArray, tokens[i]);
        //        }
        //    }
        //}

        public void SendMessage(ProtoStream protoArray, AsyncUserToken token)
        {
            if (token == null || protoArray == null)
            {
                return;
            }
            m_server.SendMessage(protoArray, token);
        }

        public void SendMessage(ProtoStream protoArray, AsyncUserToken[] tokens)
        {
            if (protoArray == null)
            {
                return;
            }

            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i] == null)
                {
                    continue;
                }

                protoArray.AddReferenceCount();
                m_server.SendMessage(protoArray, tokens[i]);
            }
        }
    }
}
