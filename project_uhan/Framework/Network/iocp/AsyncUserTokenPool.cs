﻿using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using UKon.Network;

namespace UHan.Network.iocp
{
    public sealed class AsyncUserTokenPool : IDisposable
    {
        private ConcurrentQueue<AsyncUserToken> m_queue;

        public AsyncUserTokenPool(Int32 capacity)
        {
            m_queue = new ConcurrentQueue<AsyncUserToken>();
        }

        public AsyncUserToken Pop()
        {
            if (m_queue.TryDequeue(out AsyncUserToken args))
            {
                return args;
            }
            return null;
        }

        public void Push(AsyncUserToken item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Items added to a AsyncUserToken cannot be null");
            }
            item.Clear();
            m_queue.Enqueue(item);
        }

        public void Dispose()
        {
            //while(queue.Count>0)
            //{
            //    var usertoken = Pop();
            //    usertoken.Dis
            //}
        }
    }
}
