﻿using System;
using System.Net.Sockets;
using UKon.Network;
using UKon.Proto;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IOCPConnect
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/25 9:16:26
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.Network.iocp
{

    public abstract class IOCPConnect
    {
        protected int m_bufferSize = 1024;

        protected static readonly int opsToPreAlloc = 2;

        #region Recv
        protected void AsyncRecvHead(AsyncUserToken token)
        {
            token.iIO.type = EIOOperateType.IO_READ_HEAD;
            token.RecvEventArgs.SetBuffer(token.RecvEventArgs.Offset, token.iIO.length - token.iIO.offset);

            if (token.Socket.ReceiveAsync(token.RecvEventArgs) == false)
            {
                lock (token)
                {
                    OnRecvHead(token.RecvEventArgs);
                }
            }
        }

        protected void OnRecvHead(SocketAsyncEventArgs recvEventArgs)
        {
            AsyncUserToken token = recvEventArgs.UserToken as AsyncUserToken;

            if (recvEventArgs.SocketError == SocketError.Success)
            {
                var recvSize = recvEventArgs.BytesTransferred;
                if (recvSize > 0)
                {
                    var iIO = token.iIO;

                    Array.Copy(recvEventArgs.Buffer, recvEventArgs.Offset, iIO.header, iIO.offset, recvSize);

                    iIO.offset += recvSize;

                    if (iIO.offset < iIO.length)
                    {
                        AsyncRecvHead(token);
                    }
                    else
                    {
                        if (iIO.offset > iIO.length)
                        {
                            //理论上应该不会执行到这里
                            throw new Exception("Error！ offset is bigger than length");
                        }
                        iIO.DecodeHeader();
                        iIO.offset = 0;
                        iIO.length = (int)iIO.bodyLength;
                        if (iIO.protocolId != 10100)
                        {
                            DebugMgr.LogConsole(string.Format("recv protoid={0}", iIO.protocolId));
                        }
                        //协议是否注册过,若为注册直接关闭客户端连接
                        bool result = NetworkMgr.Instance.Contains(iIO.protocolId);
                        if (result == false)
                        {
                            DebugMgr.LogConsole(string.Format("recv invalid proto, id={0}", iIO.protocolId));
                            CloseClientSocket(recvEventArgs);
                            return;
                        }
                        //包体长度无效，关闭Socket
                        if (iIO.length < 0)
                        {
                            CloseClientSocket(recvEventArgs);
                            return;
                        }
                        //包体没有内容，直接完成
                        if (iIO.length == 0)
                        {
                            OnRecvPacketComplete(recvEventArgs);

                            iIO.length = NetworkDefine.PackHeadSize;

                            AsyncRecvHead(token);
                            return;
                        }
                        iIO.body = new ByteStream(iIO.length);

                        AsyncRecvBody(token);
                    }
                }
                else
                {
                    CloseClientSocket(recvEventArgs);
                }
            }
            else
            {
                CloseClientSocket(recvEventArgs);
            }
        }

        protected void AsyncRecvBody(AsyncUserToken token)
        {
            token.iIO.type = EIOOperateType.IO_READ_BODY;
            int diff = token.iIO.length - token.iIO.offset;

            token.RecvEventArgs.SetBuffer(token.RecvEventArgs.Offset, diff > m_bufferSize ? m_bufferSize : diff);

            if (token.Socket.ReceiveAsync(token.RecvEventArgs) == false)
            {
                lock (token)
                {
                    OnRecvBody(token.RecvEventArgs);
                }
            }
        }

        protected void OnRecvBody(SocketAsyncEventArgs recvEventArgs)
        {
            AsyncUserToken token = recvEventArgs.UserToken as AsyncUserToken;

            if (recvEventArgs.SocketError == SocketError.Success)
            {
                var recvSize = recvEventArgs.BytesTransferred;
                if (recvSize > 0)
                {
                    var iIO = token.iIO;

                    Array.Copy(recvEventArgs.Buffer, recvEventArgs.Offset, iIO.body.Data, iIO.offset, recvSize);

                    iIO.offset += recvSize;
                    iIO.body.SetLength(iIO.offset);

                    if (iIO.offset < iIO.length)
                    {
                        AsyncRecvBody(token);
                    }
                    else
                    {
                        if (iIO.offset > iIO.length)
                        {
                            //理论上应该不会执行到这里
                            throw new Exception("Error！ offset is bigger than length");
                        }
                        //iIO.ParseHeader();
                        //接受到一个完成的数据包，

                        OnRecvPacketComplete(recvEventArgs);


                        iIO.length = NetworkDefine.PackHeadSize;
                        AsyncRecvHead(token);
                    }
                }
                else
                {
                    CloseClientSocket(recvEventArgs);
                }
            }
            else
            {
                CloseClientSocket(recvEventArgs);
            }
        }

        protected void OnRecvPacketComplete(SocketAsyncEventArgs recvEventArgs)
        {
            AsyncUserToken token = recvEventArgs.UserToken as AsyncUserToken;
            lock (token)
            {
                var iIO = token.iIO;
                NetworkMgr.Instance.Dispatch(iIO.protocolId, iIO.body, token);
                iIO.body?.Dispose();
                iIO.body = null;

                iIO.offset = 0;
                iIO.protocolId = 0;
                iIO.bodyLength = 0;
            }
        }
        #endregion

        #region Send

        public void SendMessage(ProtoStream stream, AsyncUserToken token)
        {
            if (token.oIO.isSend == true)
            {
                token.oIO.Push(stream);
            }
            else
            {
                SendMessage(stream.ProtoId, stream, token);
            }
        }

        private void SendMessage(UInt32 protoid, ByteStream byteArray, AsyncUserToken token)
        {
            token.oIO.protocolId = protoid;
            token.oIO.bodyLength = (UInt32)byteArray.Length;
            token.oIO.EncodeHeader();
            token.oIO.body = byteArray;
            DebugMgr.Log(string.Format("protoid = {0}, bodylen = {1} ", protoid, byteArray.Length));
            token.oIO.length = NetworkDefine.PackHeadSize;
            token.oIO.offset = 0;
            token.oIO.isSend = true;
            AsyncSendHead(token);
        }

        protected void AsyncSendHead(AsyncUserToken token)
        {
            token.oIO.type = EIOOperateType.IO_WRITE_HEAD;
            var sendEventArgs = token.SendEventArgs;
            var oIO = token.oIO;

            Buffer.BlockCopy(oIO.header, oIO.offset, sendEventArgs.Buffer, sendEventArgs.Offset, oIO.length - oIO.offset);
            sendEventArgs.SetBuffer(sendEventArgs.Offset, oIO.length - oIO.offset);

            if (token.Socket.SendAsync(sendEventArgs) == false)
            {
                lock (token)
                {
                    OnSendHead(sendEventArgs);
                }
            }
        }

        protected void OnSendHead(SocketAsyncEventArgs sendEventArgs)
        {
            AsyncUserToken token = sendEventArgs.UserToken as AsyncUserToken;

            if (sendEventArgs.SocketError == SocketError.Success)
            {
                var sendSize = sendEventArgs.BytesTransferred;
                if (sendSize > 0)
                {
                    var oIO = token.oIO;

                    oIO.offset += sendSize;

                    if (oIO.offset < oIO.length)
                    {
                        AsyncSendHead(token);
                    }
                    else
                    {
                        if (oIO.offset > oIO.length)
                        {
                            //理论上应该不会执行到这里
                            throw new Exception("Error！ offset is bigger than length");
                        }
                        oIO.offset = 0;
                        oIO.length = oIO.body.Length;

                        //包体长度无效，关闭Socket
                        if (oIO.length < 0)
                        {
                            CloseClientSocket(sendEventArgs);
                            return;
                        }
                        //包体没有内容，直接完成
                        if (oIO.length == 0)
                        {
                            OnSendPacketComplete(sendEventArgs);
                            return;
                        }
                        //oIO.body = new DynamicByteArray(iIO.length);

                        AsyncSendBody(token);
                    }
                }
                else
                {
                    CloseClientSocket(sendEventArgs);
                }
            }
            else
            {
                CloseClientSocket(sendEventArgs);
            }
        }

        protected void AsyncSendBody(AsyncUserToken token)
        {
            token.oIO.type = EIOOperateType.IO_WRITE_BODY;
            var sendEventArgs = token.SendEventArgs;
            var oIO = token.oIO;

            int diff = token.oIO.length - token.oIO.offset;
            int len = diff > m_bufferSize ? m_bufferSize : diff;

            Buffer.BlockCopy(oIO.body.Data, oIO.offset, sendEventArgs.Buffer, sendEventArgs.Offset, len);
            sendEventArgs.SetBuffer(sendEventArgs.Offset, len);

            if (token.Socket.SendAsync(sendEventArgs) == false)
            {
                lock (token)
                {
                    OnSendBody(sendEventArgs);
                }
            }
        }

        protected void OnSendBody(SocketAsyncEventArgs sendEventArgs)
        {
            AsyncUserToken token = sendEventArgs.UserToken as AsyncUserToken;

            if (sendEventArgs.SocketError == SocketError.Success)
            {
                var sendSize = sendEventArgs.BytesTransferred;
                if (sendSize > 0)
                {
                    var oIO = token.oIO;

                    oIO.offset += sendSize;

                    if (oIO.offset < oIO.length)
                    {
                        AsyncSendBody(token);
                    }
                    else
                    {
                        if (oIO.offset > oIO.length)
                        {
                            //理论上应该不会执行到这里
                            throw new Exception("Error！ offset is bigger than length");
                        }

                        OnSendPacketComplete(sendEventArgs);
                    }
                }
                else
                {
                    CloseClientSocket(sendEventArgs);
                }
            }
            else
            {
                CloseClientSocket(sendEventArgs);
            }
        }

        protected void OnSendPacketComplete(SocketAsyncEventArgs sendEventArgs)
        {
            AsyncUserToken token = sendEventArgs.UserToken as AsyncUserToken;
            var oIO = token.oIO;
            oIO.body.DelReferenceCount();
            if (oIO.body.ReferenceCount == 0)
            {
                oIO.body.Dispose();
            }
            oIO.body = null;
            oIO.offset = 0;
            oIO.isSend = false;

            ProtoStream stream = oIO.Pop();
            if (stream != null)
            {
                SendMessage(stream.ProtoId, stream, token);
            }
        }

        #endregion

        protected void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            AsyncUserToken userToken = e.UserToken as AsyncUserToken;
            userToken.ActiveDateTime = DateTime.Now;

            //try
            //{
            lock (userToken)
            {
                CCPlayerVO agent = userToken.UserToken as CCPlayerVO;
                if (e.LastOperation == SocketAsyncOperation.Receive)
                {
                    if (userToken.iIO.type == EIOOperateType.IO_READ_HEAD)
                    {
                        OnRecvHead(e);
                    }
                    else if (userToken.iIO.type == EIOOperateType.IO_READ_BODY)
                    {
                        OnRecvBody(e);
                    }
                    else
                    {
                        throw new ArgumentException("recv operation is not read type");
                    }
                }
                else if (e.LastOperation == SocketAsyncOperation.Send)
                {
                    if (userToken.oIO.type == EIOOperateType.IO_WRITE_HEAD)
                    {
                        OnSendHead(e);
                    }
                    else if (userToken.oIO.type == EIOOperateType.IO_WRITE_BODY)
                    {
                        OnSendBody(e);
                    }
                    else
                    {
                        throw new ArgumentException("recv operation is not read type");
                    }
                }
                else
                {
                    throw new ArgumentException("The last operation completed on the socket was not a receive or send");
                }
            }
            //}
            //catch (Exception)
            //{

            //}
        }


        protected virtual void CloseClientSocket(SocketAsyncEventArgs e)
        {
            //Log4Debug(String.Format("客户 {0} 断开连接!", ((Socket)e.UserToken).RemoteEndPoint.ToString()));
            AsyncUserToken token = e.UserToken as AsyncUserToken;
            CloseClientSocket(token);
        }

        protected virtual void CloseClientSocket(AsyncUserToken token)
        {
            try
            {
                token.Socket.Shutdown(SocketShutdown.Send);
            }
            catch (Exception)
            {
                // Throw if client has closed, so it is not necessary to catch.
            }
            finally
            {
                token.Socket.Close();
            }
        }

    }
}
