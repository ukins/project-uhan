﻿using ENet;
using System;
using UKon;

namespace UHan.Network.enet
{

    public class ENetServer
    {
        private Host server;
        private Address address;

        public ENetServer(){}

        public void Listen(ushort port, int peerLimit)
        {
            ENet.Library.Initialize();

            server = new Host();

            address = new Address();
            address.Port = port;

            server.Create(address, peerLimit);
        }

        public void Shutdown()
        {
            if (server != null)
            {
                server.Flush();
                ENet.Library.Deinitialize();
            }
        }

        public void Service()
        {
            Event netEvent;
            while (server.Service(0, out netEvent) > 0)
            {
                switch (netEvent.Type)
                {
                    case EventType.None:
                        break;

                    case EventType.Connect:
                        Console.WriteLine("Client connected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
                        Invoke((uint)EGenericProto.NTF_NET_DISCONNECT, netEvent.Peer, new byte[0]);
                        break;

                    case EventType.Disconnect:
                        Console.WriteLine("Client disconnected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
                        Invoke((uint)EGenericProto.NTF_NET_DISCONNECT, netEvent.Peer, new byte[0]);
                        break;

                    case EventType.Timeout:
                        Console.WriteLine("Client timeout - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
                        Invoke((uint)EGenericProto.NTF_NET_TIMEOUT, netEvent.Peer, new byte[0]);
                        break;

                    case EventType.Receive:
                        //Console.WriteLine("Packet received from - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP + ", Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length);
                        Receive(netEvent);
                        netEvent.Packet.Dispose();
                        break;
                }
            }
        }

        private void Invoke(uint protoid, Peer peer, byte[] data)
        {
            NetworkMgr.Instance.Dispatch(protoid, data, peer);
        }

        private void Receive(Event netEvent)
        {
            if (netEvent.Packet.Length < NetworkDefine.ENet_HeadSize)
            {
                return;
            }

            byte[] buffer = new byte[netEvent.Packet.Length];
            netEvent.Packet.CopyTo(buffer);

            uint protoid = BitConverter.ToUInt32(buffer, 0);

            byte[] data = new byte[netEvent.Packet.Length - NetworkDefine.ENet_HeadSize];
            Array.Copy(buffer, NetworkDefine.ENet_HeadSize, data, 0, data.Length);

            Invoke(protoid, netEvent.Peer, data);
        }

        public void Send(uint protoid, Peer peer, byte[] data, PacketFlags flags)
        {
            Packet packet = default(Packet);
            byte[] buffer = new byte[NetworkDefine.ENet_HeadSize + data.Length];

            byte[] byteType = BitConverter.GetBytes(protoid);
            Array.Copy(byteType, buffer, NetworkDefine.ENet_HeadSize);

            Array.Copy(data, 0, buffer, NetworkDefine.ENet_HeadSize, data.Length);

            packet.Create(buffer, flags);
            peer.Send(0, ref packet);
        }
    }
}
