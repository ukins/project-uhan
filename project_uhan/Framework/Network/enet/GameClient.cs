﻿using ENet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UHan.Network.enet
{
    public class GameClient
    {
        public Peer Peer { get; private set; }
        public PlayerVO VO;


        public GameClient(Peer peer)
        {
            Peer = peer;
            VO = new PlayerVO();
            VO.token = this;
        }
    }
}
