﻿using ENet;
using System;
using System.Collections.Generic;
using System.Configuration;
using UHan.Network.enet;
//using UHan.Network.iocp;
using UKon;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：NetworkMgr
// 文件功能描述：网络管理器，处理数据包的接受与发送
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/23 21:54:17
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public delegate void MessageHandler(byte[] data, GameClient peer);

    public class NetworkMgr : Singleton<NetworkMgr>, IFeature
    {
        #region IFeature

        public string Name => "NetworkMgr";


        private ENetServer m_server;

        private Dictionary<uint, GameClient> m_peer_dict = new Dictionary<uint, GameClient>();

        public void install()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (ushort.TryParse(config.AppSettings.Settings["Port"].Value, out ushort port) == false)
            {
                port = 5000;
            }
            if (int.TryParse(config.AppSettings.Settings["MaxConnectionNum"].Value, out int maxConnectionNum) == false)
            {
                maxConnectionNum = 100;
            }
            if (int.TryParse(config.AppSettings.Settings["BufferSize"].Value, out int bufferSize) == false)
            {
                bufferSize = 1024;
            }
            if (int.TryParse(config.AppSettings.Settings["SocketTimeOutMS"].Value, out int socketTimeOutMS) == false)
            {
                socketTimeOutMS = 5 * 60 * 1000;
            }

            //Register((uint)EGenericProto.NTF_NET_CONNECT, OnConnectHandler);
            //Register((uint)EGenericProto.NTF_NET_DISCONNECT, OnDisconnectHandler);
            //Register((uint)EGenericProto.NTF_NET_TIMEOUT, OnTimeOutHandler);

            TimeMgr.Instance.OnUpdate += Instance_OnUpdate;

            m_server = new ENetServer();
            m_server.Listen(port, maxConnectionNum);

            DebugMgr.LogConsole("NetworkMgr install succ");
        }


        public void uninstall()
        {

            //Unregister((uint)EGenericProto.NTF_NET_CONNECT, OnConnectHandler);
            //Unregister((uint)EGenericProto.NTF_NET_DISCONNECT, OnDisconnectHandler);
            //Unregister((uint)EGenericProto.NTF_NET_TIMEOUT, OnTimeOutHandler);
            TimeMgr.Instance.OnUpdate -= Instance_OnUpdate;

            m_server.Shutdown();

            DebugMgr.LogConsole("NetworkMgr uninstall succ");
        }
        #endregion

        private Dictionary<UInt32, MessageHandler> m_proto_dict = new Dictionary<uint, MessageHandler>();

        public Action OnTickEvent;

        public void Register(UInt32 protoid, MessageHandler func)
        {
            if (protoid == 0 || func == null)
            {
                return;
            }
            if (m_proto_dict.ContainsKey(protoid) == false)
            {
                m_proto_dict.Add(protoid, func);
            }
            else
            {
                m_proto_dict[protoid] += func;
            }
        }

        public void Unregister(UInt32 protoid, MessageHandler func)
        {
            if (protoid == 0 || func == null)
            {
                return;
            }
            if (m_proto_dict.ContainsKey(protoid) == true)
            {
                m_proto_dict[protoid] -= func;
            }
        }

        public void Dispatch(UInt32 protoid, byte[] data, Peer peer)
        {
            switch (protoid)
            {
                case (uint)EGenericProto.NTF_NET_CONNECT:
                    OnConnectHandler(data, peer);
                    break;
                case (uint)EGenericProto.NTF_NET_DISCONNECT:
                    OnDisconnectHandler(data, peer);
                    break;
                case (uint)EGenericProto.NTF_NET_TIMEOUT:
                    OnTimeOutHandler(data, peer);
                    break;
                default:
                    OnReceiveHandler(protoid, data, peer);
                    break;
            }
        }

        public bool Contains(UInt32 protoid)
        {
            return m_proto_dict.ContainsKey(protoid);
        }

        public void Send(uint protoid, uint peer_id, byte[] data, PacketFlags flags)
        {
            m_server.Send(protoid, m_peer_dict[peer_id].Peer, data, flags);
        }

        public void Send(uint protoid, uint[] peer_id_arr, byte[] data, PacketFlags flags)
        {
            for (int i = 0; i < peer_id_arr.Length; ++i)
            {
                var peer_id = peer_id_arr[i];
                m_server.Send(protoid, m_peer_dict[peer_id].Peer, data, flags);
            }
        }

        private void RemoveClient(Peer peer, byte[] data)
        {
            //Unit unit = Root.GetChild<WorldEntity>().GetUnit(peers[peer.ID].UnitID);
            //if (unit != null)
            //{
            //    unit.ClearReference();
            //    Root.GetChild<WorldEntity>().RemoveChild(unit);
            //}

            m_peer_dict.Remove(peer.ID);
        }

        #region Event Handler

        private void OnConnectHandler(byte[] data, Peer peer)
        {
            m_peer_dict.Add(peer.ID, new GameClient(peer));
        }

        private void OnDisconnectHandler(byte[] data, Peer peer)
        {
            RemoveClient(peer, data);
        }

        private void OnTimeOutHandler(byte[] data, Peer peer)
        {
            RemoveClient(peer, data);
        }
        private void OnReceiveHandler(UInt32 protoid, byte[] data, Peer peer)
        {
            if (m_proto_dict.ContainsKey(protoid) == false)
            {
                return;
            }
            if (m_peer_dict.ContainsKey(peer.ID) == false)
            {
                return;
            }

            m_proto_dict[protoid]?.Invoke(data, m_peer_dict[peer.ID]);
        }


        private void Instance_OnUpdate()
        {
            m_server.Service();
        }
        #endregion
    }
}
