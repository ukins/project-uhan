﻿using System.Collections.Generic;
using UKon;
using UKon.Proto;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ProtoMgr
// 文件功能描述：数据包解析管理器
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/29 22:39:17
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public partial class ProtoMgr : Singleton<ProtoMgr>, IFeature
    {
        private Dictionary<string, AbstractProtoDecoder> m_dict = new Dictionary<string, AbstractProtoDecoder>();

        public string Name => "ProtoMgr";

        public AbstractProtoDecoder GetProtoDecoder(string name)
        {
            string[] elements = name.Split(':');
            string newname = name;
            if (elements.Length > 1)
            {
                newname = string.Format("{0}:{1}", elements[0], elements[1].ToLower());
            }
            if (m_dict.ContainsKey(newname) == false)
            {
                return null;
            }
            return m_dict[newname];
        }

        public void install()
        {
            m_dict.Add("bool", new ProtoBoolDecoder(EProtoDecoderType.Simple));
            m_dict.Add("byte", new ProtoByteDecoder(EProtoDecoderType.Simple));
            m_dict.Add("short", new ProtoShortDecoder(EProtoDecoderType.Simple));
            m_dict.Add("ushort", new ProtoUShortDecoder(EProtoDecoderType.Simple));
            m_dict.Add("int", new ProtoIntDecoder(EProtoDecoderType.Simple));
            m_dict.Add("uint", new ProtoUIntDecoder(EProtoDecoderType.Simple));
            m_dict.Add("long", new ProtoLongDecoder(EProtoDecoderType.Simple));
            m_dict.Add("ulong", new ProtoULongDecoder(EProtoDecoderType.Simple));
            m_dict.Add("float", new ProtoFloatDecoder(EProtoDecoderType.Simple));
            m_dict.Add("string", new ProtoStringDecoder(EProtoDecoderType.Simple));

            m_dict.Add("list(bool)", new ProtoBoolDecoder(EProtoDecoderType.List));
            m_dict.Add("list(byte)", new ProtoByteDecoder(EProtoDecoderType.List));
            m_dict.Add("list(short)", new ProtoShortDecoder(EProtoDecoderType.List));
            m_dict.Add("list(ushort)", new ProtoUShortDecoder(EProtoDecoderType.List));
            m_dict.Add("list(int)", new ProtoIntDecoder(EProtoDecoderType.List));
            m_dict.Add("list(uint)", new ProtoUIntDecoder(EProtoDecoderType.List));
            m_dict.Add("list(long)", new ProtoLongDecoder(EProtoDecoderType.List));
            m_dict.Add("list(ulong)", new ProtoULongDecoder(EProtoDecoderType.List));
            m_dict.Add("list(float)", new ProtoFloatDecoder(EProtoDecoderType.List));
            m_dict.Add("list(string)", new ProtoStringDecoder(EProtoDecoderType.List));

            m_dict.Add("array(bool)", new ProtoBoolDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(byte)", new ProtoByteDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(short)", new ProtoShortDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(ushort)", new ProtoUShortDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(int)", new ProtoIntDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(uint)", new ProtoUIntDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(long)", new ProtoLongDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(ulong)", new ProtoULongDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(float)", new ProtoFloatDecoder(EProtoDecoderType.Array));
            m_dict.Add("array(string)", new ProtoStringDecoder(EProtoDecoderType.Array));

            InnerInit();

            DebugMgr.LogConsole("ProtoMgr install succ");

        }

        public void uninstall()
        {
            DebugMgr.LogConsole("ProtoMgr uninstall succ");
        }
    }
}
