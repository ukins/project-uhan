﻿using System.Collections.Generic;
using UKon;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：FeatureMgr
// 文件功能描述：模块管理器
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 21:59:30
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public class FeatureMgr : Singleton<FeatureMgr>
    {
        private Dictionary<string, IFeature> m_dict = new Dictionary<string, IFeature>();

        public void Register(IFeature feature)
        {
            if (m_dict.ContainsKey(feature.Name) == true)
            {
                DebugMgr.LogConsole(string.Format("repeat register, feature name = {0}", feature.Name));
                return;
            }
            feature.install();
            m_dict.Add(feature.Name, feature);
        }

        public void Unregister(IFeature feature)
        {
            if (m_dict.ContainsKey(feature.Name) == true)
            {
                m_dict[feature.Name].uninstall();
                m_dict.Remove(feature.Name);
            }

        }

        public void UnregisterAll()
        {
            foreach (var pair in m_dict)
            {
                pair.Value.uninstall();
            }
            m_dict.Clear();
        }

    }
}
