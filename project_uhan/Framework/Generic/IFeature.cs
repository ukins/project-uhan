﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IFeature
// 文件功能描述：模块接口
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 21:58:24
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan
{
    public interface IFeature
    {
        string Name { get; }

        void install();

        void uninstall();
    }
}
