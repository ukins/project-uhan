﻿using ENet;
using System;
using UHan;
using UHan.Common;
using UHan.flogin;
using UHan.Network.enet;
using UKon.Network;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：Agent
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 19:59:32
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Proto
{
    public partial class CCPlayerVO : IContainerElement<uint>
    {
        public static Action<CCPlayerVO> OfflineEvent;

        public uint Id { get => PlayerId; set => PlayerId = value; }

        protected GameClient m_token;
        public GameClient Token
        {
            get { return m_token; }
        }

        public CCRoomVO RoomVO;

        public PlayerMgr Mgr;

        public bool IsOnline { get; private set; }

        public void SetToken(GameClient token)
        {
            m_token = token;
        }

        public void Online(GameClient token)
        {
            this.SetToken(token);
            //token.UserToken = this;
            IsOnline = true;
        }

        public void Offline()
        {
            DebugMgr.LogConsole(string.Format("Offline {0}", Id));
            IsOnline = false;
            OfflineEvent?.Invoke(this);
        }

    }
}
