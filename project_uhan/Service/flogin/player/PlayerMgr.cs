﻿using ENet;
using System;
using System.Collections.Generic;
using UHan.Network.enet;
using UKon;
using UKon.Network;
using UKon.Proto;
using UKon.Timer;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：AgentMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 19:59:51
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.flogin
{
    public class PlayerMgr : Singleton<PlayerMgr>
    {
        #region IDictContainer
        protected Dictionary<uint, CCPlayerVO> m_dict = new Dictionary<uint, CCPlayerVO>();

        public CCPlayerVO this[uint key] => m_dict[key];

        public int Count => m_dict.Count;

        public void Add(CCPlayerVO value)
        {
            if (m_dict.ContainsKey(value.Id) == true)
            {
                return;
            }
            value.Mgr = this;
            m_dict.Add(value.Id, value);
        }

        public void Remove(uint key)
        {
            if (m_dict.ContainsKey(key) == true)
            {
                m_dict.Remove(key);
            }
        }
        #endregion

        private TimeTicker m_timer;

        //玩家过期时间
        public readonly int PeriodSeconds = 60;

        private HashSet<uint> m_offlineSet = new HashSet<uint>();

        public PlayerMgr()
        {
            //m_timer = new TimeTicker(OnTimeTickHandler, 1000);
            //m_timer.Start();
        }

        public CCPlayerVO Create(GameClient token)
        {
            var user = new CCPlayerVO();
            user.Online(token);
            user.PlayerId = token.UnitID;
            user.PlayerName = token.Name;
            Add(user);
            return user;
        }

        //private void OnTimeTickHandler(bool iscomplete)
        //{
        //    //DebugMgr.LogConsole("PlayerMgr.OnTimeTickHandler");

        //    m_offlineSet.Clear();

        //    foreach (var pair in m_dict)
        //    {
        //        var timespan = DateTime.Now - pair.Value.Token.ActiveDateTime;
        //        if (timespan.TotalSeconds > PeriodSeconds)
        //        {
        //            m_offlineSet.Add(pair.Key);
        //            pair.Value.Offline();
        //        }
        //    }

        //    foreach (var id in m_offlineSet)
        //    {
        //        Remove(id);
        //    }
        //}

    }
}
