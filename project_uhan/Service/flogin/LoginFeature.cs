﻿using ENet;
using MessagePack;
using System;
using UHan.Network.enet;
using UKon;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：LoginFeature
// 文件功能描述：登陆模块
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 10:09:11
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.flogin
{
    public class LoginFeature : IFeature
    {
        public string Name => "FLogin";

        private DatabaseMgr m_dbMgr = DatabaseMgr.Instance;

        public void install()
        {
            NetworkMgr.Instance.Register((int)ELoginProto.REQ_LOGIN, OnLoginHandler);
            NetworkMgr.Instance.Register((int)ELoginProto.REQ_CREATE, OnCreateHandler);
            NetworkMgr.Instance.Register((int)ELoginProto.REQ_SYNC, OnSyncHandler);

            //NetworkDefine.OnDisconnect += OnDisconnectHandler;

            DebugMgr.LogConsole("LoginFeature install succ");
        }

        public void uninstall()
        {
            NetworkMgr.Instance.Unregister((int)ELoginProto.REQ_LOGIN, OnLoginHandler);
            NetworkMgr.Instance.Unregister((int)ELoginProto.REQ_CREATE, OnCreateHandler);
            NetworkMgr.Instance.Unregister((int)ELoginProto.REQ_SYNC, OnSyncHandler);

            //NetworkDefine.OnDisconnect -= OnDisconnectHandler;

            DebugMgr.LogConsole("LoginFeature uninstall succ");
        }

        private void OnLoginHandler(byte[] data, GameClient peer)
        {
            LoginReq req_pack = MessagePackSerializer.Deserialize<LoginReq>(data);

            DebugMgr.Log($"proto = {ELoginProto.REQ_LOGIN}; name = {req_pack.Name}, pwd={req_pack.Pwd}");

            int errCode = 0;
            //检查name，pwd是否带非法字符 

            //检查name，pwd是否带sql注入

            string sql = $"select id from account where name='{req_pack.Name}' and pwd='{req_pack.Pwd}';";

            uint playerid = 0;
            object res = m_dbMgr.ExecuteScalar(sql);
            if (res != null)
            {
                playerid = Convert.ToUInt32(res);
                if (playerid > 0)
                {
                    errCode = 0;
                }
                else
                {
                    errCode = 1001;
                }
            }
            else
            {
                errCode = 1001;
            }

            if (errCode == 0)
            {
                peer.VO.Id = playerid;
                peer.VO.Name = req_pack.Name;
            }

            DebugMgr.Log($"proto = {ELoginProto.RES_LOGIN}; errcode = {errCode}, playerid={playerid}");

            LoginRes res_pack = new LoginRes
            {
                ErrCode = errCode,
                PlayerId = playerid,
                Name = req_pack.Name,
                Pwd = req_pack.Pwd
            };

            NetworkMgr.Instance.Send((uint)ELoginProto.RES_LOGIN, peer.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);
        }

        private void OnCreateHandler(byte[] data, GameClient peer)
        {
            CreateReq req_pack = MessagePackSerializer.Deserialize<CreateReq>(data);

            //LogMgr.Log(string.Format("name={0},pwd={1}", name, pwd));
            DebugMgr.Log($"proto = {ELoginProto.REQ_CREATE}; name = {req_pack.Name}, pwd={req_pack.Pwd}");

            int errCode = 0;
            uint playerid = 0;

            string sql = $"select count(*) from account where name='{req_pack.Name}' and pwd='{req_pack.Pwd}';";
            object res = m_dbMgr.ExecuteScalar(sql);
            if (res != null)
            {
                int cnt = Convert.ToInt32(res);
                if (cnt > 0)
                {
                    errCode = 1003;
                    goto Feedback;
                }
            }


            sql = $"insert into account(name,pwd) values('{req_pack.Name}','{req_pack.Pwd}');";
            int linecnt = m_dbMgr.ExecuteNonQuery(sql);
            if (linecnt <= 0)
            {
                errCode = 1002;
                goto Feedback;

            }

            sql =$"select id from account where name='{req_pack.Name}' and pwd='{req_pack.Pwd}';";
            res = m_dbMgr.ExecuteScalar(sql);
            if (res != null)
            {
                playerid = Convert.ToUInt32(res);
            }

            //创建成功之后自动登陆
            peer.VO.Id = playerid;
            peer.VO.Name = req_pack.Name;
        //LoginDefine.OnLoginSucc?.Invoke(token);

        Feedback:

            DebugMgr.Log($"proto = {ELoginProto.RES_CREATE}; errcode = {errCode}, playerid={playerid}");

            CreateRes res_pack = new CreateRes
            {
                ErrCode = errCode,
                PlayerId = playerid
            };

            NetworkMgr.Instance.Send((uint)ELoginProto.RES_CREATE, peer.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);
        }

        private void OnSyncHandler(byte[] data, GameClient peer)
        {
            DebugMgr.Log($"proto = {ELoginProto.REQ_SYNC}; playerid = {peer.VO.Id}, playername={peer.VO.Name}");

            SyncRes res_pack = new SyncRes
            {
                Time = TimeMgr.Instance.CurrTimeMS
            };

            NetworkMgr.Instance.Send((uint)ELoginProto.RES_SYNC, peer.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);

        }
    }
}
