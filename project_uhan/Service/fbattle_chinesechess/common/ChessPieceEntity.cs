﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon;
using UKon.AI.ChineseChess;
using UKon.Table;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ChessPieceEntity
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/14 20:28:08
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public class ChessPieceEntity : IAbstractChessPiece
    {
        protected IExAbstractChessPiece m_rule;

        #region Rule

        public EChessPieceType Chessmantype => m_rule.Chessmantype;

        public CCMoveEvent OnMoveEvent
        {
            get => m_rule.OnMoveEvent;
            set => m_rule.OnMoveEvent = value;
        }
        public CCAttackEvent OnKillEvent
        {
            get => m_rule.OnKillEvent;
            set => m_rule.OnKillEvent = value;
        }
        public CCAttackEvent OnKilledEvent
        {
            get => m_rule.OnKilledEvent;
            set => m_rule.OnKilledEvent = value;
        }

        public EChessPieceFaction Faction
        {
            get => m_rule.Faction;
            set => m_rule.Faction = value;
        }

        public AbstractChessBoard Board
        {
            get => m_rule.Board;
            set => m_rule.Board = value;
        }

        public UVector2Int CurrPos
        {
            get => m_rule.CurrPos;
        }

        public bool IsAvailable
        {
            get => m_rule.IsAvailable;
            set => m_rule.IsAvailable = value;
        }

        public List<UVector2Int> FindPoints()
        {
            return m_rule.FindPoints();
        }

        public bool CanKill(UVector2Int point)
        {
            return m_rule.CanKill(point);
        }

        public bool Kill(IAbstractChessPiece target)
        {
            return m_rule.Kill(target);
        }

        public bool Move(int x, int y)
        {
            return m_rule.Move(x, y);
        }

        public bool Move(UVector2Int vec)
        {
            return m_rule.Move(vec);
        }

        public void BeKilled(IAbstractChessPiece attacker)
        {
            m_rule.BeKilled(attacker);
        }
        #endregion

        public ChessmanTableConf Conf { get; private set; }

        public ChessPieceEntity(ChessmanTableConf conf)
        {
            Conf = conf;

            m_rule = Create(conf);

            m_rule.FocusMove(conf.BirthPoint);
        }

        private IExAbstractChessPiece Create(ChessmanTableConf conf)
        {
            EChessPieceFaction faction = conf.Id <= 16 ? EChessPieceFaction.RED : EChessPieceFaction.BLACK;
            IExAbstractChessPiece rule = null;
            switch (conf.Type)
            {
                case EChessPieceType.Soldier:
                    rule = new Soldier(faction);
                    break;
                case EChessPieceType.Cannon:
                    rule = new Cannon(faction);
                    break;
                case EChessPieceType.Chariot:
                    rule = new Chariot(faction);
                    break;
                case EChessPieceType.Knight:
                    rule = new Knight(faction);
                    break;
                case EChessPieceType.Minister:
                    rule = new Minister(faction);
                    break;
                case EChessPieceType.Scholar:
                    rule = new Scholar(faction);
                    break;
                case EChessPieceType.General:
                    rule = new General(faction);
                    break;
            }

            return rule;
        }
    }
}
