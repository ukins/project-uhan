﻿using UHan;
using UKon.AI.ChineseChess;
using UKon.Network;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：CCPlayerVO
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/16 15:00:41
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Proto
{
    public partial class CCPlayerVO : IAbstractChessPlayer
    {
        //已实现
        //public uint Id { get; set; }

        public EChessPieceFaction Faction { get; set; }

        public void OffTurn()
        {
            DebugMgr.LogConsole("OffTurn");
            ProtoStream stream = new ProtoStream((uint)EChinesechessProto.NTF_TURN);
            stream.Write(false);
            NetworkMgr.Instance.SendMessage(stream, Token);
        }

        public void OnTurn()
        {
            DebugMgr.LogConsole("OnTurn");
            ProtoStream stream = new ProtoStream((uint)EChinesechessProto.NTF_TURN);
            stream.Write(true);
            NetworkMgr.Instance.SendMessage(stream, Token);
        }

        public void KickOut()
        {
            DebugMgr.LogConsole(string.Format("KickOut {0}", Id));
            ProtoStream stream = new ProtoStream((uint)EChinesechessProto.NTF_KICK_OUT);
            stream.Write(3007u);
            NetworkMgr.Instance.SendMessage(stream, Token);
        }
    }
}
