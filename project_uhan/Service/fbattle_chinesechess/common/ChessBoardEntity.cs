﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.AI.ChineseChess;
using UKon.Table;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ChessBoardEntity
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/14 20:25:59
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public class ChessBoardEntity : AbstractChessBoard
    {
        public override void Init()
        {
            ChessmanTable table = TableMgr.Instance.ChessmanTableVO;
            var lst = table.FindAllVO();
            foreach (var conf in lst)
            {
                var chessman = new ChessPieceEntity(conf);
                chessman.Board = this;
                chessman.IsAvailable = true;
                m_dict.Add(conf.Id, chessman);
            }
        }

        public override void Init(ChineseChessSnapshot snapshot)
        {

        }

        public override ChineseChessSnapshot Snapshot()
        {
            throw new NotImplementedException();
        }

        public override void Start()
        {
            foreach (var pair in m_dict)
            {
                pair.Value.OnKilledEvent += OnChessPieceBeKilledHandler;
            }
        }

        public override void End()
        {
            foreach (var pair in m_dict)
            {
                pair.Value.OnKilledEvent += OnChessPieceBeKilledHandler;
            }
        }

        public override void SelectChessman(IAbstractChessPiece chessman)
        {
            throw new NotImplementedException();
        }

        public override void DeselectChessman()
        {
            throw new NotImplementedException();
        }

        private void OnChessPieceBeKilledHandler(IAbstractChessPiece attacker, IAbstractChessPiece defender)
        {
            if (defender.Chessmantype == EChessPieceType.General)
            {
                OnFinishEvent?.Invoke(attacker, defender);
            }
        }

    }
}
