﻿using UKon.AI.ChineseChess;
using UKon.Timer;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：CCRoomVO
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/8 22:18:21
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public class ChinessChessMatch
    {
        public long StartTime { get; private set; }

        private DelayTicker m_ready_timer;
        public long CurrServerTime => TimeMgr.Instance.CurrTimeMS;

        private RoomVO m_roomvo;
        public ChessStateEntity State { get; private set; }

        public bool IsOnTurn(uint playerid)
        {
            var player = State.GetPlayer(playerid);
            if (player.Faction == State.Player)
            {
                return true;
            }
            return false;
        }

        public ChinessChessMatch(RoomVO vo)
        {
            m_roomvo = vo;
            vo.match = this;

            m_ready_timer = new DelayTicker(OnReadyTickHandler, 1000 * 5);
            TimeMgr.Instance.AddTick(m_ready_timer);
        }

        public void StartMatch()
        {
            m_roomvo.State = ERoomState.Ready;

            //比赛开始时间，当前服务器时间加上5秒
            StartTime = CurrServerTime + 5 * 1000;

            m_ready_timer.Start();

            State = new ChessStateEntity();

            State.SetPlayer(m_roomvo.Master, EChessPieceFaction.RED);
            State.SetPlayer(m_roomvo.Challenger, EChessPieceFaction.BLACK);

            ChessBoardEntity board = new ChessBoardEntity();
            board.Init();
            State.SetBoard(board);

            State.SetFirst(EChessPieceFaction.RED);

            State.Start();
        }

        public void Finish()
        {
            m_ready_timer.Stop();
            m_ready_timer = null;
            State.End();
        }

        private void OnReadyTickHandler(bool iscomplete)
        {
            DebugMgr.Log("OnReadyTickHandler");
            if (CurrServerTime >= StartTime)
            {
                DebugMgr.Log("Start Match");

                var red = State.GetPlayer(EChessPieceFaction.RED);
                var black = State.GetPlayer(EChessPieceFaction.BLACK);
                red.OnTurn();
                black.OffTurn();
            }
        }
    }
}
