﻿using UKon.AI.ChineseChess;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ChessStateEntity
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/14 20:18:50
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public class ChessStateEntity : ChineseChessState
    {
        public override EChessPieceFaction GetSelfFaction(uint playerid)
        {
            IAbstractChessPlayer redplayer = GetPlayer(EChessPieceFaction.RED);
            if (redplayer != null && redplayer.Id == playerid)
            {
                return EChessPieceFaction.RED;
            }
            return EChessPieceFaction.BLACK;
        }
    }
}
