﻿using UKon.AI.ChineseChess;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：HumanPlayerEntity
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/14 20:17:48
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public class HumanPlayerEntity : HumanPlayer
    {
        protected PlayerVO m_vo;
        public PlayerVO VO { get { return m_vo; } }

        public HumanPlayerEntity(PlayerVO vo)
        {
            m_vo = vo;
        }

        public override void OnTurn()
        {
        }

        public override void OffTurn()
        {
        }
    }
}
