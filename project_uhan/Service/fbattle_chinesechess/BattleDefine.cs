﻿#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：BattleDefine
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/8 22:00:33
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.fchinesechess
{
    public delegate void BattleEvent(RoomVO vo);
    public class BattleDefine
    {
        public static BattleEvent OnStartMatch;
    }
}
