﻿using ENet;
using MessagePack;
using UHan.flobby;
using UHan.Network.enet;
using UKon;

namespace UHan.fchinesechess
{
    public class BattleFeature : IFeature
    {
        public string Name => "FBattle_ChinessChess";

        public void install()
        {
            BattleDefine.OnStartMatch += OnStartMatchHandler;
            NetworkMgr.Instance.Register((int)EChinesechessProto.REQ_SELECT, OnSelectHandler);
            NetworkMgr.Instance.Register((int)EChinesechessProto.REQ_MOVE, OnMoveHandler);
            NetworkMgr.Instance.Register((int)EChinesechessProto.REQ_KILL, OnKillHandler);

            DebugMgr.LogConsole("BattleFeature install succ");
        }

        public void uninstall()
        {
            BattleDefine.OnStartMatch -= OnStartMatchHandler;
            NetworkMgr.Instance.Unregister((int)EChinesechessProto.REQ_SELECT, OnSelectHandler);
            NetworkMgr.Instance.Unregister((int)EChinesechessProto.REQ_MOVE, OnMoveHandler);
            NetworkMgr.Instance.Unregister((int)EChinesechessProto.REQ_KILL, OnKillHandler);

            DebugMgr.LogConsole("BattleFeature uninstall succ");
        }


        #region Event Handler
        /// <summary>
        /// 开始比赛
        /// </summary>
        /// <param name="vo"></param>
        private void OnStartMatchHandler(RoomVO vo)
        {
            ChinessChessMatch match = new ChinessChessMatch(vo);
            match.StartMatch();

            StartMatchNtf ntf_pack = new StartMatchNtf
            {
                StartTime = match.StartTime,
                VO = vo
            };

            NetworkMgr.Instance.Send((uint)ELobbyProto.NTF_START_MATCH, vo.TokenArr,
                MessagePackSerializer.Serialize(ntf_pack), PacketFlags.Reliable);
        }
        /// <summary>
        /// 选中棋子
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnSelectHandler(byte[] data, GameClient agent)
        {
            uint chessid = MessagePackSerializer.Deserialize<uint>(data);

            DebugMgr.Log($"proto = {EChinesechessProto.REQ_SELECT}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, chessid={chessid}");

            RoomVO roomvo = null;
            PlayerVO competitor = null;
            ChinessChessMatch match = null;

            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            roomvo = agent.VO.RoomVO;
            if (roomvo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (roomvo.Master == null || roomvo.Challenger == null)
            {
                errCode = 2004;
                goto Feedback;
            }

            match = roomvo.match;
            if (match == null)
            {
                errCode = 3000;
                goto Feedback;
            }

            competitor = roomvo.Master.Id == agent.VO.Id ? roomvo.Challenger : roomvo.Master;

            if (chessid > 0)
            {
                ChessPieceEntity chesspiece = match.State.Board[chessid] as ChessPieceEntity;
                if (chesspiece == null)
                {
                    errCode = 3001;
                    goto Feedback;
                }
                if (chesspiece.Faction != match.State.GetSelfFaction(agent.VO.Id))
                {
                    errCode = 3002;
                    goto Feedback;
                }
            }

        Feedback:
            SelectRes res_pack = new SelectRes
            {
                ErrCode = errCode,
                Chessid = chessid
            };
            NetworkMgr.Instance.Send((uint)EChinesechessProto.RES_SELECT, agent.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);

            if (errCode == 0)
            {
                SelectNtf ntf_pack = new SelectNtf
                {
                    Chessid = chessid
                };
                NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_SELECT, competitor.token.Peer.ID,
                    MessagePackSerializer.Serialize(ntf_pack), PacketFlags.Reliable);
            }

        }

        /// <summary>
        /// 移动棋子
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnMoveHandler(byte[] data, GameClient agent)
        {
            ChessVO vo = MessagePackSerializer.Deserialize<ChessVO>(data);

            DebugMgr.Log($"proto = {EChinesechessProto.REQ_MOVE}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, chessvo={vo}");

            RoomVO roomvo = null;
            PlayerVO competitor = null;
            ChinessChessMatch match = null;

            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            roomvo = agent.VO.RoomVO;
            if (roomvo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (roomvo.Master == null || roomvo.Challenger == null)
            {
                errCode = 2004;
                goto Feedback;
            }

            match = roomvo.match;
            if (match == null)
            {
                errCode = 3000;
                goto Feedback;
            }

            competitor = roomvo.Master.Id == agent.VO.Id ? roomvo.Challenger : roomvo.Master;

            ChessPieceEntity chesspiece = match.State.Board[vo.Id] as ChessPieceEntity;
            if (chesspiece == null || chesspiece.IsAvailable == false)
            {
                errCode = 3001;
                goto Feedback;
            }
            if (chesspiece.Faction != match.State.GetSelfFaction(agent.VO.Id))
            {
                errCode = 3002;
                goto Feedback;
            }

            //是否已超过准备时间
            if (match.CurrServerTime < match.StartTime)
            {
                errCode = 3005;
                goto Feedback;
            }

            //当前是否轮到自己
            if (match.IsOnTurn(agent.VO.Id) == false)
            {
                errCode = 3006;
                goto Feedback;
            }

            bool result = chesspiece.Move(vo.X, vo.Y);
            if (result == false)
            {
                errCode = 3003;
                goto Feedback;
            }

        Feedback:
            MoveRes res_pack = new MoveRes
            {
                ErrCode = errCode,
                VO = vo
            };
            NetworkMgr.Instance.Send((uint)EChinesechessProto.RES_MOVE, agent.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);

            if (errCode == 0)
            {
                NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_MOVE, agent.Peer.ID,
                    MessagePackSerializer.Serialize(vo), PacketFlags.Reliable);

                match.State.SwitchPlayer();
            }
        }

        /// <summary>
        /// 击杀棋子
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnKillHandler(byte[] data, GameClient agent)
        {
            KillReq req_pack = MessagePackSerializer.Deserialize<KillReq>(data);

            DebugMgr.Log($"proto = {EChinesechessProto.REQ_SELECT}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, KillReq={req_pack}");

            RoomVO roomvo = null;
            PlayerVO competitor = null;
            ChinessChessMatch match = null;

            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            roomvo = agent.VO.RoomVO;
            if (roomvo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (roomvo.Master == null || roomvo.Challenger == null)
            {
                errCode = 2004;
                goto Feedback;
            }

            match = roomvo.match;
            if (match == null)
            {
                errCode = 3000;
                goto Feedback;
            }

            competitor = roomvo.Master.Id == agent.VO.Id ? roomvo.Challenger : roomvo.Master;

            ChessPieceEntity attacker = match.State.Board[req_pack.Attackerid] as ChessPieceEntity;
            ChessPieceEntity defender = match.State.Board[req_pack.Defenderid] as ChessPieceEntity;
            if (attacker == null || defender == null)
            {
                errCode = 3001;
                goto Feedback;
            }
            if (attacker.Faction != match.State.GetSelfFaction(agent.VO.Id)
                || defender.Faction != match.State.GetSelfFaction(competitor.Id))
            {
                errCode = 3002;
                goto Feedback;
            }

            //是否已超过准备时间
            if (match.CurrServerTime < match.StartTime)
            {
                errCode = 3005;
                goto Feedback;
            }

            //当前是否轮到自己
            if (match.IsOnTurn(agent.VO.Id) == false)
            {
                errCode = 3006;
                goto Feedback;
            }

            bool result = attacker.Kill(defender);
            if (result == false)
            {
                errCode = 3004;
                goto Feedback;
            }

        Feedback:
            KillRes res_pack = new KillRes
            {
                ErrCode = errCode,
                Attackerid = req_pack.Attackerid,
                Defenderid = req_pack.Defenderid,
            };
            NetworkMgr.Instance.Send((uint)EChinesechessProto.RES_KILL, agent.Peer.ID,
               MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);

            if (errCode == 0)
            {
                KillNtf ntf_pack = new KillNtf
                {
                    Attackerid = req_pack.Attackerid,
                    Defenderid = req_pack.Defenderid,
                };
                NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_KILL, competitor.token.Peer.ID,
                   MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);

                if (match.State.IsGameOver() == false)
                {
                    match.State.SwitchPlayer();
                }
                else
                {
                    var winner = match.State.Winner;
                    bool isagentwin = agent.VO.Faction == winner;

                    NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_GAME_OVER, agent.Peer.ID,
                       MessagePackSerializer.Serialize(isagentwin), PacketFlags.Reliable);

                    NetworkMgr.Instance.Send((uint)EChinesechessProto.NTF_GAME_OVER, competitor.token.Peer.ID,
                       MessagePackSerializer.Serialize(isagentwin == false), PacketFlags.Reliable);

                    RoomMgr.Instance.Remove(roomvo.Id);
                    agent.VO.RoomVO = null;
                    competitor.RoomVO = null;
                }
            }
        }

        #endregion
    }
}
