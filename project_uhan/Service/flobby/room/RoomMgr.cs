﻿using System;
using System.Collections.Generic;
using System.Linq;
using UKon;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：RoomMgr
// 文件功能描述：游戏房间管理器
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 20:30:52
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.flobby
{
    public class RoomMgr : Singleton<RoomMgr>
    {
        #region IDictContainer
        private Dictionary<uint, RoomVO> m_dict = new Dictionary<uint, RoomVO>();

        public RoomVO this[uint key] => m_dict[key];

        public int Count => m_dict.Count;

        public List<RoomVO> FindAll() => m_dict.Values.ToList();

        public void Add(RoomVO value) => m_dict.Add(value.Id, value);

        public void Remove(uint key) => m_dict.Remove(key);
        #endregion

        private static readonly uint N = 10;
        private static readonly uint MAX = 999999;
        private uint[] startArray = new uint[N];
        private HashSet<uint> m_roomIdSet = new HashSet<uint>();

        private uint FindNew(uint key)
        {
            uint temp = key;
            while (true)
            {
                if (m_roomIdSet.Contains(key) == true)
                {
                    key++;
                    key = key % MAX;
                    if (temp == key)
                    {
                        return uint.MaxValue;
                    }
                }
                else
                {
                    return key;
                }
            }
        }

        private uint GeneRoomId()
        {
            Random rand = new Random();
            for (int i = 0; i < N; i++)
            {
                int seed = rand.Next(100);
                seed = seed % 10;

                uint tem = startArray[seed];
                startArray[seed] = startArray[i];
                startArray[i] = tem;
            }
            uint result = 0;
            for (int i = 0; i < 6; i++)
            {
                result = result * 10 + startArray[i];
            }
            if (m_roomIdSet.Contains(result) == true)
            {
                result = FindNew(result);
                if (result == uint.MaxValue)
                {
                    DebugMgr.LogConsole("Error! RoomId Broken");
                }
                else
                {
                    m_roomIdSet.Add(result);
                }
            }
            else
            {
                m_roomIdSet.Add(result);
            }
            return result;
        }

        public RoomMgr()
        {
            for (uint i = 0; i < N; i++)
            {
                startArray[i] = i;
            }
            m_roomIdSet.Clear();
        }

        public RoomVO Create()
        {
            var roomid = GeneRoomId();
            if (roomid <= 0)
            {
                return null;
            }
            var vo = new RoomVO();
            vo.Id = roomid;
            vo.Mgr = this;
            Add(vo);
            return vo;
        }

        public void Recycle(RoomVO vo)
        {
            if (m_roomIdSet.Contains(vo.Id) == false)
            {
                return;
            }
            m_roomIdSet.Remove(vo.Id);
            Remove(vo.Id);
        }

    }
}
