﻿using System.Collections.Generic;
using UHan.Common;
using UHan.fchinesechess;
using UHan.flobby;
using UHan.Network.enet;
using UKon.Network;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：Room
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 20:30:34
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Proto
{
    public partial class CCRoomVO : IContainerElement<uint>
    {
        public static readonly int MAX_MEMBER_CNT = 2;

        public uint Id { get => Roomid; set => Roomid = value; }

        public RoomMgr Mgr;

        public CCRoomVO()
        {
            Players = new List<CCPlayerVO>();
        }

        public CCPlayerVO Master
        {
            get
            {
                if (Players.Count > 0)
                {
                    return Players[0];
                }
                return null;
            }
        }

        public CCPlayerVO Challenger
        {
            get
            {
                if (Players.Count > 1)
                {
                    return Players[1];
                }
                return null;
            }
        }

        public CCPlayerVO Applier;

        public GameClient[] TokenArr
        {
            get
            {
                var arr = new GameClient[2];
                arr[0] = Master?.Token;
                arr[1] = Challenger?.Token;
                return arr;
            }
        }

        public ChinessChessMatch match;

        public void CreateBy(CCPlayerVO player)
        {
            Roomstate = ERoomState.Wait;
            player.RoomVO = this;
            Players.Clear();
            Players.Add(player);

            CCPlayerVO.OfflineEvent += OnOfflineHandler;
        }

        public void Join(CCPlayerVO player)
        {
            player.RoomVO = this;
            Players.Add(player);
        }

        private void OnOfflineHandler(CCPlayerVO player)
        {
            player.RoomVO?.Disband();
        }

        private void Disband()
        {
            CCPlayerVO.OfflineEvent -= OnOfflineHandler;
            foreach (var player in Players)
            {
                player.RoomVO = null;
                if (player.IsOnline == true)
                {
                    player.KickOut();
                }
            }
            Mgr.Remove(Id);
        }
    }
}
