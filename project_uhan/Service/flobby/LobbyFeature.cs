﻿using ENet;
using MessagePack;
using System.Linq;
using UHan.fchinesechess;
using UHan.Network.enet;
using UKon;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：LobbyFeature
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/26 14:58:19
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UHan.flobby
{
    public class LobbyFeature : IFeature
    {
        public string Name => "FLobby";

        private readonly DatabaseMgr m_dbMgr = DatabaseMgr.Instance;
        //private readonly PlayerMgr m_agentMgr = PlayerMgr.Instance;
        private readonly RoomMgr m_roomMgr = RoomMgr.Instance;

        public void install()
        {
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_GET_ROOM_LIST, OnReqGetRommListHandler);
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_GET_ROOM, OnReqGetRoomVOHandler);
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_CREATE, OnCreateRoomHandler);
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_QUIT_ROOM, OnQuitRoomHandler);
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_JOIN, OnJoinRoomHandler);
            NetworkMgr.Instance.Register((int)ELobbyProto.REQ_DEAL_JOIN, OnDealJoinRoomHandler);

            DebugMgr.LogConsole("LobbyFeature install succ");
        }

        public void uninstall()
        {
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_GET_ROOM_LIST, OnReqGetRommListHandler);
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_GET_ROOM, OnReqGetRoomVOHandler);
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_CREATE, OnCreateRoomHandler);
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_QUIT_ROOM, OnQuitRoomHandler);
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_JOIN, OnJoinRoomHandler);
            NetworkMgr.Instance.Unregister((int)ELobbyProto.REQ_DEAL_JOIN, OnDealJoinRoomHandler);

            DebugMgr.LogConsole("LobbyFeature uninstall succ");
        }

        /// <summary>
        /// 获取房间列表信息
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnReqGetRommListHandler(byte[] data, GameClient agent)
        {
            DebugMgr.Log($"proto = {ELobbyProto.REQ_GET_ROOM_LIST}; playerid = {agent.VO.Id}, playername={agent.VO.Name}");

            //后续分页处理
            var rooms = m_roomMgr.FindAll();

            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_GET_ROOM_LIST, agent.Peer.ID,
                MessagePackSerializer.Serialize(rooms), PacketFlags.Reliable);
        }

        /// <summary>
        /// 获取单个房间信息
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnReqGetRoomVOHandler(byte[] data, GameClient agent)
        {
            uint roomid = MessagePackSerializer.Deserialize<uint>(data);

            DebugMgr.Log($"proto = {ELobbyProto.REQ_GET_ROOM}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, roomid={roomid}");

            int errCode = 0;
            var vo = m_roomMgr[roomid];
            if (vo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

        Feedback:
            GetRoomVORes res_pack = new GetRoomVORes
            {
                Errcode = errCode,
                VO = vo
            };

            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_GET_ROOM, agent.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);
        }

        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnCreateRoomHandler(byte[] data, GameClient agent)
        {
            DebugMgr.Log($"proto = {ELobbyProto.REQ_CREATE}; playerid = {agent.VO.Id}, playername={agent.VO.Name}");

            var roomvo = agent.VO.RoomVO;
            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            if (roomvo == null)
            {
                roomvo = m_roomMgr.Create();
            }

            if (roomvo == null)
            {
                errCode = 2001;
                goto Feedback;
            }

            roomvo.CreateBy(agent.VO);

        Feedback:
            CreateRoomRes res_pack = new CreateRoomRes
            {
                Errcode = errCode,
                VO = roomvo
            };

            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_CREATE, agent.Peer.ID,
                MessagePackSerializer.Serialize(res_pack), PacketFlags.Reliable);
        }

        /// <summary>
        /// 退出房间
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnQuitRoomHandler(byte[] data, GameClient agent)
        {
            DebugMgr.Log($"proto = {ELobbyProto.REQ_QUIT_ROOM}; playerid = {agent.VO.Id}, playername={agent.VO.Name}");

            RoomVO roomvo = null;
            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            roomvo = agent.VO.RoomVO;
            if (roomvo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (agent.VO != roomvo.Master)
            {
                errCode = 2006;
                goto Feedback;
            }

            if (roomvo.State != ERoomState.Wait)
            {
                errCode = 2007;
                goto Feedback;
            }

        Feedback:

            if (errCode == 0)
            {
                m_roomMgr.Recycle(roomvo);
                agent.VO.QuitRoom();
            }
            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_CREATE, agent.Peer.ID,
                MessagePackSerializer.Serialize(errCode), PacketFlags.Reliable);
        }
        /// <summary>
        /// 请求加入房间
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnJoinRoomHandler(byte[] data, GameClient agent)
        {
            uint roomid = MessagePackSerializer.Deserialize<uint>(data);

            DebugMgr.Log($"proto = {ELobbyProto.REQ_JOIN}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, roomid={roomid}");

            RoomVO roomvo = null;
            int errCode = 0;
            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            roomvo = m_roomMgr[roomid];
            if (roomvo == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (roomvo.State != ERoomState.Wait)
            {
                errCode = 2003;
                goto Feedback;
            }

            if (roomvo.Master == null)
            {
                errCode = 2004;
                goto Feedback;
            }

            PlayerVO applier = roomvo.Applier;
            if (applier != null)
            {
                if (applier.Id == agent.VO.Id)
                {
                    errCode = 2008;
                }
                else
                {
                    errCode = 2009;
                }
                goto Feedback;
            }

        Feedback:

            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_JOIN, agent.Peer.ID,
                MessagePackSerializer.Serialize(errCode), PacketFlags.Reliable);

            if (errCode == 0)
            {
                roomvo.Applier = agent.VO;

                NetworkMgr.Instance.Send((uint)ELobbyProto.NTF_JOIN, roomvo.Master.token.Peer.ID,
                    MessagePackSerializer.Serialize(agent.VO), PacketFlags.Reliable);
            }
        }

        /// <summary>
        /// 处理加入申请
        /// </summary>
        /// <param name="bytearray"></param>
        /// <param name="token"></param>
        private void OnDealJoinRoomHandler(byte[] data, GameClient agent)
        {
            bool isaccept = MessagePackSerializer.Deserialize<bool>(data);
            DebugMgr.Log($"proto = {ELobbyProto.REQ_DEAL_JOIN}; playerid = {agent.VO.Id}, playername={agent.VO.Name}, isaccept={isaccept}");

            PlayerVO applier = null;

            bool isready = false;
            int errCode = 0;

            if (agent == null)
            {
                errCode = 2000;
                goto Feedback;
            }

            if (agent.VO.RoomVO == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            if (agent.VO.RoomVO.Applier == null)
            {
                errCode = 2002;
                goto Feedback;
            }

            applier = agent.VO.RoomVO.Applier;

            if (isaccept == true)
            {
                agent.VO.RoomVO.Join(applier);
                isready = true;
            }
            else
            {
                agent.VO.RoomVO.Applier = null;
            }

        Feedback:
            var roomvo = agent.VO.RoomVO;

            NetworkMgr.Instance.Send((uint)ELobbyProto.RES_DEAL_JOIN, agent.Peer.ID,
                MessagePackSerializer.Serialize(errCode), PacketFlags.Reliable);

            if (errCode == 0)
            {
                DealJoinNtf ntf_pack = new DealJoinNtf
                {
                    isaccept = isaccept,
                    VO = agent.VO
                };
                NetworkMgr.Instance.Send((uint)ELobbyProto.NTF_DEAL_JOIN, applier.token.Peer.ID,
                    MessagePackSerializer.Serialize(ntf_pack), PacketFlags.Reliable);

                if (isready == true)
                {
                    BattleDefine.OnStartMatch?.Invoke(agent.VO.RoomVO);
                }
            }
        }
    }
}
